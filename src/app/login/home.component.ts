import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtHelper } from "angular2-jwt";

const httpOptions = {
  headers: new HttpHeaders({ "Content-type": "application/json" })
};
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
}) 
export class HomeComponent implements OnInit {
  private apiURL = "http://192.168.1.69:44392/api/accountmanagement/authenticate";
  //private apiURL = "https://localhost:44392/api/accountmanagement/authenticate";

  // private apiURL = "https://localhost:44392/api/accountmanagement/authenticate";
  invalidLogin: boolean;
  messages: string;
  userformGroup: FormGroup;
  unamePattern = "^[a-z0-9_-]{8,15}$";

  constructor(
    private http: HttpClient,
    private router: Router,
    private jwtHelper: JwtHelper,
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }

  ngOnInit() {

  }

  createForm(){
    this.userformGroup = this.formBuilder.group({
      userName : ['',[Validators.required, Validators.minLength(10)]],
      passWord : ['',Validators.required,],
    });
  }


  login(username?: string, password?: string) {
    this.http
      .post(
        this.apiURL,
        { username, password },
        {
          headers: new HttpHeaders({
            "Content-Type": "application/json"
          })
        }
      )
      .subscribe(
        response => {
          let token = (<any>response).token;
          localStorage.setItem("jwt", token);
          this.invalidLogin = false;
          var tokendecode = this.jwtHelper.decodeToken(token);
          console.log(tokendecode);
          
          switch (tokendecode.role) {
            case "Admin": {
              this.router.navigate(["/admin/dashboard"]);
              break;
            }
            case "Customer": {
              this.router.navigate(["/customer/book-room"]);
              break;
            }
            case "FloorManager": {
              this.router.navigate(["/floormanager"]);
              break;
            }
          }
        },
        err => {
          this.messages = err.error.message;
          // console.log(this.messages);
          this.invalidLogin = true;
      }
    );
  }
}
