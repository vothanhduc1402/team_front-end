import { MngFloorsComponent } from './admin/manage-buildings/mng-floors/mng-floors.component';
import { MngBuildingsComponent } from './admin/manage-buildings/mng-buildings/mng-buildings.component';


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/auth.interceptor';
import { JwtHelper } from 'angular2-jwt'
import { AuthGuardService } from './guard/auth-guard.service';


import { HomeComponent } from './login/home.component';
import { ErrorComponent } from './error/error.component';
import { AdminComponent } from './admin/admin.component';
import { FloorComponent } from './floor/floor.component';
import { CustomerComponent } from './customer/customer.component';
import { TableListComponent } from './admin/table-list/table-list.component';
import { ScheduleCalendarComponent } from './admin/schedule-calendar/schedule-calendar.component';
import { BuildingsComponent } from './booked_Room/buildings/buildings.component';
import { FloorsComponent } from './booked_Room/floors/floors.component';
import { RoomsComponent } from './booked_Room/rooms/rooms.component';
import { FloorMngFloorsComponent } from './floor/floor-manager-buildings/floor-mng-floors/floor-mng-floors.component';
import { FloorMngRoomsComponent } from './floor/floor-manager-buildings/floor-mng-rooms/floor-mng-rooms.component';
import { MngRoomsComponent } from './admin/manage-buildings/mng-rooms/mng-rooms.component';
import { DashboardAdminComponent } from './admin/dashboard-admin/dashboard-admin.component';
import { DetailCustomerComponent } from './customer/detail-customer/detail-customer.component';
import { ScheduleCalendarCustomerComponent } from './customer/schedule-calendar-customer/schedule-calendar-customer.component';
import { BookingScheduleComponent } from './booking-schedule/booking-schedule.component';

import { FloormanagerScheduleCalendarComponent } from './floor/floormanager-schedule-calendar/floormanager-schedule-calendar.component';
import { ScheduleApproveComponent } from './admin/schedule-approve-admin/schedule-approve.component';
import { FloormanagerScheduleApproveComponent } from './floor/floormanager-schedule-approve/floormanager-schedule-approve.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },

  {
    path: 'admin', canActivate: [AuthGuardService],
    //  canActivate: [RoleGuard],
    // data: {
    //   expectedRole: 'admin'
    // },
    children: [
    { path: 'dashboard', component: DashboardAdminComponent,  },

    { path: 'user-management', component: TableListComponent,  },
    { path: 'user-management/adduser', component: TableListComponent,  },

    {path:  'manage-buildings', component: MngBuildingsComponent, },
    {path:  'manage-buildings/manage-floors/:id', component: MngFloorsComponent},
    {path:  'manage-buildings/manage-floors/addfloor/:id', component: MngFloorsComponent},
    {path:  'manage-buildings/manage-floors/manage-room/:id/:id_floor', component: MngRoomsComponent},
    {path:  'manage-buildings/manage-floors/manage-room/addroom/:id/:id_floor', component: MngRoomsComponent},
    {path:  'manage-buildings/addbuilding', component: MngBuildingsComponent, },  

    {path:  'manage-buildings/manage-floors', component: MngFloorsComponent}, 

    { path: 'Schedule-Calendar', component: BookingScheduleComponent },
    { path: 'Schedule-Calendar/add-schedule', component: BookingScheduleComponent },
    { path: 'Schedule-Calendar/floors/:id', component: FloorsComponent },
    { path: 'Schedule-Calendar/floors/rooms/:id_building/:id_floor', component: RoomsComponent },
    { path: 'Schedule-Calendar/floors/rooms/schedule-calendar/:id_building/:id_floor/:id_room', component: ScheduleCalendarComponent },
    {path: 'schedule-approve', component: ScheduleApproveComponent}
  ],
    component: AdminComponent
  },
  {
    path: 'floormanager',  canActivate: [AuthGuardService],
    children: [
      {path: 'floor-manage-floors', component: FloorMngFloorsComponent},
      {path: 'floor-manage-floors/:id/:idfloor', component: FloorMngRoomsComponent},
      {path: 'floor-manage-floors/addroom/:id/:idfloor', component: FloorMngRoomsComponent},
      {path: 'floor-manage-floors/Schedule-Calendar', component: FloormanagerScheduleCalendarComponent},
      {path: 'floor-manage-floors/Schedule-Approve', component: FloormanagerScheduleApproveComponent},
    ],
    component: FloorComponent
  }, 
  {
    path: 'customer',  canActivate: [AuthGuardService],
    children:[
      { path: 'detail', component: DetailCustomerComponent,  },
      { path: 'book-room', component: BookingScheduleComponent,  },
      { path: 'book-room/floors/:id', component: FloorsComponent },
      { path: 'book-room/floors/rooms/:id_building/:id_floor', component: RoomsComponent },
    ],
    component: CustomerComponent
  },
  {
    path: '**',
    component: ErrorComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    JwtHelper, 
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi : true,
    }
  
  ],
})
export class AppRoutingModule { }
