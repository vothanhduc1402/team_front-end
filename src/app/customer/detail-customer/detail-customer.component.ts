import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { User } from 'app/models/user';
import { GetUserIdService } from 'app/shared/get-user-id.service';

@Component({
  selector: 'app-detail-customer',
  templateUrl: './detail-customer.component.html',
  styleUrls: ['./detail-customer.component.css']
})
export class DetailCustomerComponent implements OnInit {

  user = new User();

  constructor(private userService: UserService, private getUserIdService: GetUserIdService) { }

  ngOnInit() {
    this.GetCurrentCustomer();
  }

  GetCurrentCustomer(): any{
    this.userService.getUserfromId(this.getUserIdService.GetUserIdFromLogin()).subscribe(
      (data: User) => {
        this.user = data;
      }
    );
  }
  
}
