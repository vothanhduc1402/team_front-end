import { Component, OnInit } from '@angular/core';
import { AuthGuardService } from 'app/guard/auth-guard.service';

@Component({
  selector: 'app-navbar-customer',
  templateUrl: './navbar-customer.component.html',
  styleUrls: ['./navbar-customer.component.css']
})
export class NavbarCustomerComponent implements OnInit {

  constructor(private authGuard: AuthGuardService) { }

  ngOnInit() {
  }

  LogOut(){
    localStorage.removeItem("jwt");
    this.authGuard.canActivate();
  }
}
