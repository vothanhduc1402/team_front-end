import {
  Component,
  NgModule,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  ChangeDetectorRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  setHours
} from 'date-fns';
import { Subject, of, forkJoin } from 'rxjs';
import { NgbModal, NgbModalRef,NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import {BookingService} from '../../services/booking.service';
import {CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, CalendarWeekViewAllDayEvent, CalendarEventTitleFormatter} from 'angular-calendar';
import { Schedule } from 'app/models/schedule';
import { Router, ActivatedRoute} from '@angular/router';
import { Building } from './../../models/building';
import  {BuildingService} from '../../services/building.service'; 
import { Floor } from 'app/models/floor';
import { Room } from 'app/models/room';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NgbModalBackdrop } from '@ng-bootstrap/ng-bootstrap/modal/modal-backdrop';
import Swal from 'sweetalert2';
import { tap, catchError } from 'rxjs/operators';
import { ObserveOnSubscriber } from 'rxjs/internal/operators/observeOn';
import { JwtHelper } from 'angular2-jwt';
import { Deal } from 'app/models/deal';
import { BookingCustomerService } from 'app/services/booking-customer.service';
import { concat } from 'rxjs/observable/concat';









const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  gray: {
    primary: '#000000',
    secondary: '#C6CACE'
  },
  green: {
    primary: '#1d6318',
    secondary: '#93f48d'
  }
  
};

@Component({
  selector: 'app-schedule-calendar-customer',
  templateUrl: './schedule-calendar-customer.component.html',
  styleUrls: ['./schedule-calendar-customer.component.css']
})
export class ScheduleCalendarCustomerComponent implements OnInit {

 

 setColors(statusSchedule:string, idSchedule:string) : any{
    if(statusSchedule == "waiting approved" && this.listScheduleFromIDUser.filter(sche => sche.id_schedule == idSchedule)[0] == null)
      return colors.yellow;
    else if(statusSchedule == "waiting approved" && this.listScheduleFromIDUser.filter(sche => sche.id_schedule == idSchedule)[0] != null)
      return colors.yellow;
    else if(statusSchedule == "approved" && this.listScheduleFromIDUser.filter(sche => sche.id_schedule == idSchedule)[0] == null)
      return colors.blue;
    else if(statusSchedule == "approved" && this.listScheduleFromIDUser.filter(sche => sche.id_schedule == idSchedule)[0] != null){
      return colors.green;
    }
 }

  id_building : string;
  id_floor: string;
  id_room: string = "";

  id_building_schedule: string;

  idBuilding : string = null;
  idFloor : string = null;
  idRoom : string = null;
  arrayBuilding : Building[];
  arrayFloor : Floor[] = [];
  arrayRoom : Room[];
  openButton: boolean;
  arrayIDbuilding : string[] = [];
  calendar: CalendarWeekViewAllDayEvent;
  dayCurrent = new Date();
  nrSelect = 0;
  hourStart : number;
  hourEnd : number;
  minuteStart : number;
  rentHour : number;
  arrayHour : number[] = [];
  arrayRentHour: number[] = [];
  modalReference = null; 
  checkTimeLesserCurrent = null;
  selectedEvent : CalendarEvent;
  selectedTimeStart : Date;
  selectedTimeEnd : Date;
  scheduleUpdate = new Schedule();
  id_user: string;
  user_name:string;

  // calendar: CalendarWeekViewAllDayEvent;

  constructor(private dialog: MatDialog ,private modal: NgbModal, private jwtHelper:JwtHelper,private BookingService : BookingService, private BookingServiceCustomer: BookingCustomerService,private BuildingService: BuildingService,private _ActivatedRoute: ActivatedRoute, private _router: Router) {
  }
  

  ngOnInit() {

    this.getAllBuilding();
    this.openButton = true;
    this.setHour();
    this.setRentHour();
    this.GetIdUserFromLogin();
    this.GetUserNameFromLogin();
    this.getScheduleFromIDUser();
  }
  
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  @ViewChild('modalAddSchedule') modalAddSchedule: TemplateRef<any>;
  

  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;

  

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            this.events = this.events.filter(iEvent => iEvent !== event);
            this.deleteEvent(event);           
            this.refresh.next();
            console.log(this.listSchedule);
            
          }
        })
        
      }
    }
  ];

  
  refresh: Subject<any> = new Subject();
  listSchedule: Schedule[];
  listScheduleFromFloor: Schedule[];
  listScheduleFromFloorByWeek: Schedule[];
  listScheduleFromRoomByWeek: Schedule[];
  listScheduleFromIDUser: Schedule[];
  listNameRoom:Room[];
  selected = 0;
  events: CalendarEvent[] = [
    
    
  ];

  getScheduleFromIDUser(){
    this.BookingServiceCustomer.getScheduleFromIDUser(this.id_user).pipe(
      tap((schedule: Schedule[])=> console.log(`schedule from id user: ${JSON.stringify(schedule)}`))
      ).subscribe(
      (data: Schedule[]) => {
          this.listScheduleFromIDUser = data;

      }
      
      )
  }
  

  
  activeDayIsOpen: boolean = true;
  GetIdUserFromLogin(){

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);
    this.id_user = tokenDecode.unique_name;

    return this.id_user;
  }

  GetUserNameFromLogin(){

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);
    this.user_name = tokenDecode.actort;

    return this.user_name;
  }


  getAllBuilding(){
    this.BuildingService.getAllBuilding().subscribe(
      (data : Building[]) =>{
        console.log(data);
        this.arrayBuilding = data;
      }
    )
  }

  onSelectBuilding(idBuilding: string){
    this.arrayFloor = [];
    this.arrayRoom = [];
    this.selected = 0;
    this.BuildingService.getAllFloorByIDBuilding(idBuilding).subscribe(
      (data : Floor[]) =>{
        
        this.arrayFloor = data;

      }
    )

    this.events = [];
    this.openButton = true;
    this.id_building = idBuilding;
    this.idBuilding = idBuilding;
    this.idFloor = null; 
    this.idRoom = null;
 
  }

  dateofweek:Date;
  onSelectFloor(idBuilding: string,idFloor: string){
    this.events = [];
    this.arrayRoom = [];
    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    this.BuildingService.getAllRoomByIDFloor(idBuilding, idFloor).subscribe(
      (data : Room[]) =>{
        console.log(data);
        this.arrayRoom = data;

      }
    );
    // this.events = [];
    // this.getAllScheduleFromFloor(idBuilding, idFloor);
    // this.dateofweek = new Date(this.viewDate);
    // this.getScheduleFloorByWeek(idFloor, this.dateofweek.toISOString());
    // this.openButton = true;
    // this.id_floor = idFloor;
    // this.id_room = null;
    // console.log(this.id_room);
    
    // this.idFloor = idFloor;
    // this.nrSelect = 0;
    // this.idRoom = null;
    if(day1 < day2){
      this.events = [];
     this.getScheduleFloorByWeek(idFloor, this.dateofweek.toISOString());
      this.id_floor = idFloor;
      this.id_room = null;
    }
    else{
      this.events = [];
      this.getAllScheduleFromFloor(idBuilding, idFloor, this.dateofweek);
      this.dateofweek = new Date(this.viewDate);
     // this.getScheduleFloorByWeek(idFloor, this.dateofweek.toISOString());
      this.openButton = true;
      this.id_floor = idFloor;
      this.id_room = null;
      console.log(this.id_room);
      
      this.idFloor = idFloor;
      this.nrSelect = 0;
      this.idRoom = null;
    }
    this.nrSelect = 0;


  }

  onSelectRoom(idRoom: string){
    if(idRoom == "0"){
      this.onSelectFloor(this.idBuilding, this.idFloor);
    }
    else{
      this.openButton = false;
      this.events = [];
      this.getAllScheduleByRoom(idRoom);
      this.dateofweek = new Date(this.viewDate);
      this.getScheduleRoomByWeek(this.id_floor, idRoom, this.dateofweek.toISOString());
      this.id_room = idRoom;
      this.idRoom = idRoom;
      console.log(idRoom);
    }
    
      
  }
  
  onWeekPrevious(){
    
    
    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    this.events = [];
      if(day1 == day2 && this.id_room == null){
        this.onSelectFloor(this.id_building, this.id_floor);
      }
      else if(day1 == day2 && this.id_room != null){
        this.events = [];
        this.onSelectRoom(this.id_room);
      }
      else if(day1 > day2 && this.id_room == null){
        this.events = [];
        this.onSelectFloor(this.id_building, this.id_floor);
      }
      else if(day1 > day2 && this.id_room != null){
        this.events = [];
        this.onSelectRoom(this.id_room);
      }
      else if(this.id_room != null){
        this.events = [];
        this.getScheduleRoomByWeek(this.id_floor,this.id_room, this.dateofweek.toISOString())
      }
      else{
        this.events = [];
        this.getScheduleFloorByWeek(this.id_floor, this.dateofweek.toISOString());
      }
      
      
      
      //this.getScheduleFloorByWeek(this.id_floor, this.dateofweek.toISOString());
      
   
     
    
 
 

    
  }
  
  onWeekNext(){
    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    this.events = [];
    if(day1 == day2 && this.id_room == null){
      this.events = [];
      this.onSelectFloor(this.id_building, this.id_floor);

    }
    else if(day1 == day2 && this.id_room != null){
      this.events = [];
      this.onSelectRoom(this.id_room);
    }
    else if(day1 < day2 && this.id_room != null){
      this.events = [];
      this.getScheduleRoomByWeek(this.id_floor,this.id_room, this.dateofweek.toISOString());
     
    }

    else if(day1 < day2 && this.id_room == null){
      this.events = [];
      this.getScheduleFloorByWeek(this.id_floor, this.dateofweek.toISOString());
      
    }
    else if(day1 > day2 && this.id_room == null){
      this.onSelectFloor(this.idBuilding, this.idFloor);
    }
    else if(day1 > day2 && this.id_room != null){
      this.onSelectRoom(this.id_room);
    }

  }
  onToday(){
    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    this.events = [];
   if(day1 == day2 && this.id_room == null){
    this.events = [];
    this.onSelectFloor(this.id_building, this.id_floor);
   }
   else if(this.id_room != null){
    this.events = [];
    this.onSelectRoom(this.id_room);
   }
  
    
  }

  addSchedule(dateChoosed?: Date, hourChoose?: number, minuteChoose?: string, timeSchedule?: number){
   
    this.messagesAddSchedule = null;
     var minute = Number(minuteChoose);
     var dateStart = new Date(dateChoosed);
      // dateChoosed.setHours(15);
      dateStart.setHours(hourChoose);
       dateStart.setMinutes(minute);
       
       var dateFinish = new Date(dateStart);
       dateFinish.setHours(hourChoose+timeSchedule);
       // console.log(dateStart);
       // console.log(dateFinish);
       // console.log(this.id_Building);
       // console.log(this.id_Floor);
       // console.log(this.id_Room);
       // console.log(this.id_user);
 
       var schedules : Schedule[]  = [];
       var schedule = new Schedule();
       schedule.id_building = this.id_building;
       schedule.id_floor = this.id_floor;
       schedule.id_room = this.id_room;
       schedule.time_start = dateStart;
       schedule.time_finish = dateFinish;
       schedule.date = dateChoosed;
       schedule.id_personapprove = "";
       schedule.issue = "";
       schedule.person_create = this.user_name;
       schedule.person_update = this.user_name;

       schedules.push(schedule);
 
       var deal = new Deal();
       deal.id_user = this.id_user;
       deal.person_create = this.user_name;
       deal.person_update = this.user_name;
       deal.schedule = schedules;

       
     
       if(hourChoose == null || minuteChoose == null || timeSchedule == null){
         this.messagesAddSchedule = "Please enter full information";
       }
       else{

       this.BookingServiceCustomer.createDealScheduleCustomer(deal).pipe(
        tap((deal: Deal)=> console.log(`deal: ${JSON.stringify(deal)}`),
        (err: any) => this.messagesAddSchedule = err.error.message),
        catchError(error => of(null))
        ).subscribe(insertedDeal => {

             
            if(this.messagesAddSchedule === null){
                this.modalReference.close();
                Swal.fire({
                    type: 'success',
                    title: 'Booking schedule success',
                    showConfirmButton: false,
                    timer: 1500
                  });

                this.listScheduleFromIDUser = [];
                this.getScheduleFromIDUser();
                this.events=[];
                this.onSelectRoom(this.idRoom);

            }
          
           }
           
       
         
         
      
    
       
       )
       
       
      
       
   }
 }
 
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }
  eventClicked({ event }: { event: CalendarEvent }): void {
    console.log('Event clicked', event);
    // this.handleEvent('Dropped or resized', event);
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      var dateNow = new Date();
      var newDateStart = newStart;
      var newDateEnd = newEnd;
      //backup event when close modal
      if(event.id == iEvent.id){
        this.selectedEvent = iEvent;
        this.selectedTimeStart = iEvent.start;
        this.selectedTimeEnd = iEvent.end;       
      }
         
      if (iEvent === event && newDateStart >= dateNow ) {
            if(this.CheckScheduleForTimeStart(event.id.toString(),newDateStart,newDateEnd) == true || 
              this.CheckScheduleForTimeEnd(event.id.toString(),newDateStart,newDateEnd) == true  ||
              this.CheckScheduleForTimeFull(event.id.toString(),newDateStart,newDateEnd) == true){
              Swal.fire(
                'Time conflict',
                '',
                'warning'
              );
            }
            else if((newDateEnd.getHours() - newDateStart.getHours()) > 5 ||
            (newDateEnd.getHours() - newDateStart.getHours()) <= -7 && 
            (newDateEnd.getHours() - newDateStart.getHours()) > -19){
                  Swal.fire(
                    'Book room no more than 5 hours',
                    '',
                    'warning'
                  )
            }
            else {
              event.start = newStart;
              event.end = newEnd;
              this.handleEvent('Dropped or resized', event);
              return {
                ...event,
                id : event.id,
                start: newStart,
                end: newEnd,
                draggable: true
              };
            }     
      }      
      else if(iEvent === event && newDateStart < dateNow){
        Swal.fire(
          'Not book before the current date',
          '',
          'warning'
        )
        return {
          ...event,
          id : event.id,
          start: iEvent.start,
          end: iEvent.end,
          draggable: true
        };
      }
      return iEvent;
    });
    // handle hour start, minute start and hour end for modal update schedule
    this.hourStart = Number(event.start.getHours());
    this.minuteStart = Number(event.start.getMinutes());
    this.hourEnd = Number(event.end.getHours());
    if(this.hourEnd == 0){
      this.hourEnd = 24;
    }
    this.rentHour = Number(this.hourEnd-event.start.getHours());
    Promise.resolve(null).then(() => this.hourStart = Number(event.start.getHours()));
    Promise.resolve(null).then(() => this.minuteStart = Number(event.start.getMinutes()));
    // this.checkTimeLesserCurrent = null;
    // var dateNow = new Date();
    // if(dateNow.getDate() > event.start.getDate() ){
    //   this.checkTimeLesserCurrent = "Not selected before today";
    // }
    
  }


  // handleEvent(action: string, event: CalendarEvent): void {
  //   this.modalData = { event, action };
  //   this.modal.open(this.modalContent, { size: 'lg' });
  // }
    objtest : any;
   addEvent(){
    // this.events = [
    //   ...this.events,
    //   {
    //     title: 'New event',
    //     start: startOfDay(new Date()),
    //     end: endOfDay(new Date()),
    //     color: colors.red,
    //     actions: this.actions,
    //     draggable: true,
    //     resizable: {
    //       beforeStart: true,
    //       afterEnd: true
    //     }
    //   }
    // ];
    this.objtest = {
      title: 'New event 1',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        actions: this.actions,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
        
        
      }
    this.events.push(this.objtest);
    this.refresh.next();
    console.log("event");
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    // this.BookingService.deleteScheduleFromFloor(eventToDelete.id.toString()).subscribe();
    this.BookingService.updateStatusDeal(eventToDelete.id.toString(),"closed").subscribe();
    this.events = this.events.filter(event => event !== eventToDelete);
    // update listschedule
    var sche = this.listSchedule.filter(sche => sche.id_schedule == eventToDelete.id)[0];
    this.listSchedule.splice(this.listSchedule.indexOf(sche), 1);

  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }


  
 obj:any;
  getAllScheduleByRoom(id_roomSchedule: string){
    this.BookingService.getScheduleFromIdRoom(id_roomSchedule).subscribe(
      (data: Schedule[]) => {

       // console.log(`All schedule = ${JSON.stringify(data)}`);
        this.listSchedule = data;
        
        for(var i=0; i< this.listSchedule.length; i++){
          this.obj ={
            id : this.listSchedule[i].id_schedule,
            start : new Date(this.listSchedule[i].time_start),
            end : new Date(this.listSchedule[i].time_finish),
            title : "<b>" +"R." + this.listNameRoom.filter(room => room.id_room == this.listSchedule[i].id_room)[0].name_room,
            color :  this.setColors(this.listSchedule[i].status, this.listSchedule[i].id_schedule),
            actions: this.actions,
            resizable : {
              beforeStart: (this.listScheduleFromIDUser.some(sche => sche.id_schedule == this.listSchedule[i].id_schedule))?true:false,
              afterEnd: (this.listScheduleFromIDUser.some(sche => sche.id_schedule == this.listSchedule[i].id_schedule))?true:false
            },
            draggable : (this.listScheduleFromIDUser.some(sche => sche.id_schedule == this.listSchedule[i].id_schedule))?true:false
          }

            this.events.push(this.obj);
          
        }
        this.refresh.next();
      //  console.log(`List schedule = ${JSON.stringify(this.listSchedule)}`);
       // console.log(`List calendar schedule = ${JSON.stringify(this.listCalendar)}`);
      }
    )
  }

 



  protected objScheduleOfFloor:any;
  getAllScheduleFromFloor(idBuilding: string, idFloor:string, date: Date){
  
    const combine = forkJoin(this.BookingService.getNameRoom(idBuilding, idFloor),
      this.BookingService.getScheduleFromFloor(idBuilding, idFloor),
      this.BookingService.getScheduleFromFloorByWeek(idFloor, date.toISOString())).subscribe(
        ([res1, res2, res3]) => {
          this.listNameRoom = res1;
          this.listScheduleFromFloor = res2;
          this.listScheduleFromFloorByWeek = res3;
          for(var i=0; i< this.listScheduleFromFloor.length; i++){
                      this.objScheduleOfFloor ={
                        start : new Date(this.listScheduleFromFloor[i].time_start),
                        end : new Date(this.listScheduleFromFloor[i].time_finish),
                        title : "<b>" + "R." +  + this.listNameRoom.filter(room => room.id_room == this.listScheduleFromFloor[i].id_room)[0].name_room + "</b>",
                        color :  (this.listScheduleFromFloor[i].status == "approved") ? colors.blue:colors.yellow,
                      allDay :  false,
                      
                        resizable : {
                          beforeStart: false,
                          afterEnd: false
                        },
                      draggable : false}
        
                        this.events.push(this.objScheduleOfFloor);
                      
                    }

                    
          for(var i=0; i< this.listScheduleFromFloorByWeek.length; i++){
            this.objScheduleOfFloorByWeek ={
              start : new Date(this.listScheduleFromFloorByWeek[i].time_start),
              end : new Date(this.listScheduleFromFloorByWeek[i].time_finish),
              title : "<b>" +"R." +  this.listNameRoom.filter(room => room.id_room == this.listScheduleFromFloorByWeek[i].id_room)[0].name_room,
              color :  colors.gray,
             allDay :  false,
             
              resizable : {
                beforeStart: false,
                afterEnd: false
              },
             draggable : false}
  
              this.events.push(this.objScheduleOfFloorByWeek);
            
          }



                this.refresh.next();
               //console.log(`List schedule = ${JSON.stringify(this.listSchedule)}`);
               // console.log(`List calendar schedule = ${JSON.stringify(this.listCalendar)}`);
                  
              
        }
      )
  }
  objScheduleOfFloorByWeek:any;
  getScheduleFloorByWeek(id_Floor:string, dayofweek:string){
      this.BookingService.getScheduleFromFloorByWeek(id_Floor, dayofweek).subscribe(
        (data: Schedule[]) => {
          //console.log(`All histoty schedule from floor = ${JSON.stringify(data)}`);
          this.listScheduleFromFloorByWeek = data;
          for(var i=0; i< this.listScheduleFromFloorByWeek.length; i++){
            this.objScheduleOfFloorByWeek ={
              start : new Date(this.listScheduleFromFloorByWeek[i].time_start),
              end : new Date(this.listScheduleFromFloorByWeek[i].time_finish),
              title : "<b>" +"R." +  this.listNameRoom.filter(room => room.id_room == this.listScheduleFromFloorByWeek[i].id_room)[0].name_room,
              color :  colors.gray,
             allDay :  false,
             
              resizable : {
                beforeStart: false,
                afterEnd: false
              },
             draggable : false}
  
              this.events.push(this.objScheduleOfFloorByWeek);
            
          }
          this.refresh.next();


        }


      )
  }

  objScheduleOfRoomByWeek:any;
  getScheduleRoomByWeek(id_Floor:string, id_Room:string, dayofweek:string){

      this.BookingService.getScheduleFromRoomByWeek(id_Floor, id_Room, dayofweek).subscribe(
        (data: Schedule[]) => {
        // console.log(`All histoty schedule from room = ${JSON.stringify(data)}`);
          this.listScheduleFromRoomByWeek = data;
          for(var i=0; i< this.listScheduleFromRoomByWeek.length; i++){
            this.objScheduleOfRoomByWeek ={
              start : new Date(this.listScheduleFromRoomByWeek[i].time_start),
              end : new Date(this.listScheduleFromRoomByWeek[i].time_finish),
              title : "<b>" +"R." + this.listNameRoom.filter(room => room.id_room == this.listScheduleFromRoomByWeek[i].id_room)[0].name_room,
              color :  colors.gray,
             allDay :  false,
             
              resizable : {
                beforeStart: false,
                afterEnd: false
              },
             draggable : false}
  
              this.events.push(this.objScheduleOfRoomByWeek);
            
          }
          this.refresh.next();


        }


      )
  }
  modalAddOption: NgbModalOptions = {}; // not null!
  messagesAddSchedule = null;
  modalAddEvent(): void {
    this.modalAddOption.backdrop = 'static';
    this.modalAddOption.keyboard = false;
    this.modalAddOption.size = "lg";
    this.modalReference = this.modal.open(this.modalAddSchedule,this.modalAddOption);
    this.messagesAddSchedule = null;
  }
  
  modalOption: NgbModalOptions = {}; // not null!
  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    this.modalOption.size = "lg";
    this.modalReference = this.modal.open(this.modalContent,this.modalOption);
    this.getScheduleById(this.selectedEvent.id.toString());
    this.messagesUpdateSchedule = null;
  }
  setHour(){
    for(let i = 6; i < 24; i++){
      this.arrayHour.push(i);
    }
  }

  setRentHour(){
    for(let i = 1; i < 6; i++){
      this.arrayRentHour.push(i);
    }
  }


  getScheduleById(idSchedule: string){
    let currentSchedule = new Schedule();
    this.BookingService.getScheduleById(idSchedule).subscribe(
      (data: Schedule) => {
        currentSchedule = data;
        this.scheduleUpdate = currentSchedule; 
        console.log(data);
        console.log(this.scheduleUpdate);
        
               
      }
    );
    console.log(this.scheduleUpdate);
  }
  messagesUpdateSchedule : string ;
  updateModal(idBuilding : string, idFloor: string, idRoom: string, dateChoosed: Date,hourChoose: number,
    minuteChoose : string, timeSchedule : number){
    // if(this.checkTimeLesserCurrent == null)
      console.log(idRoom);
      
      this.messagesUpdateSchedule = null;
      var minute = Number(minuteChoose);
      var dateStart = new Date(dateChoosed);
      dateStart.setHours(hourChoose);
      dateStart.setMinutes(minute);    
      var dateFinish = new Date(dateStart);
      dateFinish.setHours(hourChoose+timeSchedule);
      this.scheduleUpdate.id_building = idBuilding;
      this.scheduleUpdate.id_floor = idFloor;
      this.scheduleUpdate.id_room = idRoom;
      this.scheduleUpdate.time_start = dateStart;
      this.scheduleUpdate.time_finish = dateFinish;
      console.log(this.scheduleUpdate);
      if(idBuilding == null || idFloor == null || idRoom == null){
        this.messagesUpdateSchedule = "Please enter full information";
      }
      else{
        this.BookingService.updateSchedule(this.selectedEvent.id.toString(),this.scheduleUpdate)
        .pipe(
          tap((schedule: Schedule)=> console.log(`schedule: ${JSON.stringify(schedule)}`),
          (err: any) => {
            this.messagesUpdateSchedule = err.error.message;
            console.log(err);
            
          }),
          catchError(error => of(null))
          )
        .subscribe(x => {
          if(this.messagesUpdateSchedule == null){
            this.modalReference.close();
            this.onSelectRoom(idRoom);
            // Swal.fire({
            //   position: 'center',
            //   type: 'success',
            //   title: 'Your work has been saved',
            //   showConfirmButton: false,
            //   timer: 1500
            // });
            
          }
        });

          
       
      }
      
  }
  closeModal(){
    this.events = this.events.map(iEvent => {
      if (iEvent.id == this.selectedEvent.id){
        return {
          ...this.selectedEvent,
          start: this.selectedTimeStart,
          end: this.selectedTimeEnd,
        };
      }
      return iEvent;
    });   
    this.refresh.next();

    this.modalReference.close();
    
  }
  closeModalAdd(){
    this.modalReference.close();
  }
  // CheckScheduleForTimeStart(id : string,start : Date, end: Date) : boolean {
  //     for(let sche of this.listSchedule){
  //       if(sche.id_schedule != id){
  //           if(start >=  new Date(sche.time_start)  && start < new Date(sche.time_finish )){                     
  //             return true;
  //           }
  //       }  
  //     }
  //     return false;
  // }
  // CheckScheduleForTimeEnd(id : string,start : Date, end: Date) : boolean {
  //   for(let sche of this.listSchedule){
  //     if(sche.id_schedule != id){
  //         if(end >  new Date(sche.time_start)  && end <= new Date(sche.time_finish) ){         
  //           return true;
  //         }
  //     }
  //   }
  //   return false;
  // }
  // CheckScheduleForTimeFull(id : string,start : Date, end: Date) : boolean {
  //   for(let sche of this.listSchedule){
  //     if(sche.id_schedule != id){
  //         if(start <  new Date(sche.time_start)  && end >= new Date(sche.time_finish) ){         
  //           return true;
  //         }
  //     }
  //   }
  //   return false;
  // }
  CheckScheduleForTimeStart(id : string,start : Date, end: Date) : boolean {
    for(let sche of this.events){
      if(sche.id != id){
          if(start >=  new Date(sche.start)  && start < new Date(sche.end )){                     
            return true;
          }
      }  
    }
    return false;
}
CheckScheduleForTimeEnd(id : string,start : Date, end: Date) : boolean {
  for(let sche of this.events){
    if(sche.id != id){
        if(end >  new Date(sche.start)  && end <= new Date(sche.end) ){         
          return true;
        }
    }
  }
  return false;
}
CheckScheduleForTimeFull(id : string,start : Date, end: Date) : boolean {
  for(let sche of this.events){
    if(sche.id != id){
        if(start <  new Date(sche.start)  && end >= new Date(sche.end) ){         
          return true;
        }
    }
  }
  return false;
}

 
}
