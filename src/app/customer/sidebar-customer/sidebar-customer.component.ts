import { Validators } from '@angular/forms';
import { FormControl, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { User } from 'app/models/user';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { GetUserIdService } from 'app/shared/get-user-id.service';
import Swal from 'sweetalert2';
import { ViewChild, ElementRef} from '@angular/core';


@Component({
  selector: 'app-sidebar-customer',
  templateUrl: './sidebar-customer.component.html',
  styleUrls: ['./sidebar-customer.component.css']
})
export class SidebarCustomerComponent implements OnInit {

  passwordFormGroup: FormGroup;
  user : User;
  userId: string;
  errorMessageFromChangePassword: string = null;
  @ViewChild('closeChangePass') closeChangePass: ElementRef;
  constructor(private formBuilder: FormBuilder, 
              private userService: UserService, 
              private getUserIdService: GetUserIdService) {

    this.passwordFormGroup = this.formBuilder.group({
      password: ['', Validators.required],
      repeatpassword: ['',Validators.required,],
    })

   }

  ngOnInit() {
    this.userId = this.getUserIdService.GetUserIdFromLogin();
    console.log(this.userId);
    this.GetCurrentCustomer();
  }

  GetCurrentCustomer(): any{
    this.userService.getUserfromId(this.getUserIdService.GetUserIdFromLogin()).subscribe(
      (data: User) => {
        this.user = data;
        console.log(this.user);
      }
    );
  }

  OnUpdateCustomer(user_new: User, username: string, oldPassword:string, newPassword: string){
    this.errorMessageFromChangePassword = null;
    user_new.username = username;
    user_new.password = newPassword;

    if(user_new == null || username == "" || oldPassword == "" || newPassword == ""){
          this.errorMessageFromChangePassword = "Please enter full information";
    }
    else{
    this.userService.ChangePasswordUser(oldPassword,newPassword,username,this.user).pipe(
      tap((user: User) => console.log(`update password:  ${JSON.stringify(user)}`),
      err => this.errorMessageFromChangePassword = err.error.message
      ),
      catchError(error => of(new User())))
      .subscribe(_ =>{
        if(this.errorMessageFromChangePassword == null){
          this.OnClose(); 
          Swal.fire({
            type: 'success',
            title: 'Your password has been updated',
            showConfirmButton: false,
            timer: 1500
          });
        } 
      });
    }
  }
  OnClose() {
    this.closeChangePass.nativeElement.click();
  }
}
