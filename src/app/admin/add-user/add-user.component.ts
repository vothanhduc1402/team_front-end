

import { FormControl, Validators,FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, Output, HostListener, EventEmitter } from '@angular/core';
import { Building } from 'app/models/building';


import { MatDialog } from "@angular/material";
import { UserService } from 'app/services/user.service';
import { User } from 'app/models/user';
import { JwtHelper } from "angular2-jwt";
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import  Swal  from 'sweetalert2';
import { Router } from '@angular/router';
import { Directive } from '@angular/core';



@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})



export class AddUserComponent implements OnInit {
  
  capOn;
  // maxDate = new Date(2000, 0, 1);
  currentDate = new Date();

  year = this.currentDate.getFullYear();
  month = this.currentDate.getMonth();
  day = this.currentDate.getDay()

  myDate = new Date(this.currentDate);
 
  listUser: User[] = [];
  levels: string[] = ['Admin', 'FloorManager', 'Customer'];
  arrayBuildingFloor : Building[];
  arrayFloorByBuilding = new Building();
  public selectedValue: string;
  setDis:boolean;
  UserName: string;
  checkUserNameAndEmail: string = null; 
  capsOn;
  
  email = new FormControl('', [Validators.required, Validators.email]);
  nameFormControl = new FormControl('',[Validators.required, Validators.minLength(6), Validators.maxLength(30)]);
  passwordFormControl = new FormControl('',[Validators.required, Validators.minLength(6), Validators.maxLength(30)]);
  userNameFormControl = new FormControl('',[Validators.required]);
  phoneFormControl = new FormControl('',[Validators.required]);
  birthdayFormControl = new FormControl('',[Validators.required]);
  
  registerForm: FormGroup;
  submitted = false;
  
  constructor(private dialog: MatDialog, 
              private userService: UserService, 
              private jwtHelper: JwtHelper,
              private router: Router,
              private formBuilder: FormBuilder ) { }



  

  ngOnInit() { 
    this.getAllBuildingFloor();
    this.GetUserNameFromLogin();

    this.myDate.setFullYear(this.year - 18);
    this.myDate.setMonth(this.month + 1);
    this.myDate.setDate(this.day);
    
    // this.registerForm = this.formBuilder.group({
    //   nameFormControl: ['', Validators.required,Validators.minLength(6), Validators.maxLength(30)],
    //   passwordFormControl: ['', Validators.required, Validators.minLength(6), Validators.maxLength(30)],
    //   email: ['', [Validators.required, Validators.email]],
    //   userNameFormControl: ['', [Validators.required]],
    //   phoneFormControl: ['', [Validators.required]],
    //   birthdayFormControl: ['', [Validators.required]]
  // });

  }

  onClose(){
    this.dialog.closeAll();
  }

  onSave(username: string, 
    password: string, 
    level: string, 
    id_building: string,
    id_floors : string[]=[], 
    name: string, 
    email: string, 
    phone: string, 
    birthday: Date){
      this.submitted = true;
      this.checkUserNameAndEmail = null;
      var newUser : User = new User()
        newUser.username = username;
        newUser.password = password;
        newUser.level = level;
        newUser.id_building = id_building;
        newUser.id_floors = id_floors;
        newUser.id = "";
        newUser.token = "";
        newUser.person_create = this.UserName; // get UserName from method GetUserNameFormlogin()
        newUser.name = name;
        newUser.email = email;
        newUser.phone = phone;
        newUser.birthday = birthday;
        
        if(newUser == null || username =="" || password =="" || level =="" || id_building =="" || id_floors == [] || name =="" || email =="" || phone =="" || birthday == null){
          this.checkUserNameAndEmail = "Please enter full information";
        }
        else {
        this.userService.addUser(newUser).pipe(
          tap((user: User) => console.log(`inserted user = ${JSON.stringify(user)}`),
          (err) => this.checkUserNameAndEmail = err.error.message),
          catchError(err2 => of(new User()))
        ).
        subscribe(insertedUser => {
          this.listUser.push(insertedUser) ; 
          if(this.checkUserNameAndEmail === null){
            this.onClose(); 
            Swal.fire({
              type: 'success',
              title: 'Your user has been add',
              showConfirmButton: false,
              timer: 1500
            });
            this.router.navigateByUrl('/admin/user-management');
        }   
      })
        }
  }
  
  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter email like abc@email.com' :
        this.email.hasError('email') ? 'Not a valid email' : '';
  }

  GetErrorMessageName(){
    return this.nameFormControl.hasError('required') ? 'Name is required' :
      this.nameFormControl.hasError('minLength') || this.nameFormControl.hasError('maxLength') ? '' : 'Sorry, your name must be between 6 and 30 characters long';
  }

  GetErrorMessagePassword(){
    return this.passwordFormControl.hasError('required') ? 'Password is required' :
     this.passwordFormControl.hasError('minLength') || this.passwordFormControl.hasError('maxLength') ? '' : 'Sorry, your password must be between 6 and 30 characters long';
  }
  
  GetErrorMessagePhone(){
    return this.phoneFormControl.hasError('required') ? 'Phone is required or must be at most 10 digits' : '';
      // this.phoneFormControl.hasError('maxLength') ? '' : 'text wrong';
  }

  
  
  

  selectChangeHandler(level: string){   
    if(level == "FloorManager"){
      this.setDis = false;  
    }  
    else{
      this.setDis = true;
    }    
  }

  GetUserNameFromLogin(){

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);    
    this.UserName = tokenDecode.actort; 

    return this.UserName;
  }

  getAllBuildingFloor(){
    this.userService.getNameAllBuildingFloor().subscribe(
      (data : Building[]) =>{
        console.log(data);
        this.arrayBuildingFloor = data;
      }
    )
  }
  
  onSelectBuilding(idBuilding: string){
    this.arrayFloorByBuilding = this.arrayBuildingFloor.filter(building => building.id == idBuilding)[0];
    console.log(`idBuilding = ${JSON.stringify(idBuilding)}`);
    console.log(`array filterd = ${JSON.stringify(this.arrayFloorByBuilding)}`);
  }
}


@Directive({ selector: '[capsLock]' })
export class TrackCapsDirective {
  @Output('capsLock') capsLock = new EventEmitter<Boolean>();
  
  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): void {
    this.capsLock.emit(event.getModifierState && event.getModifierState('CapsLock'));
  }
  @HostListener('window:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent): void {
    this.capsLock.emit(event.getModifierState && event.getModifierState('CapsLock'));
  }
}

