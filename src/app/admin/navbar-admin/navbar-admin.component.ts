import { AuthGuardService } from './../../guard/auth-guard.service';
import { Component, OnInit } from '@angular/core';
import { JwtHelper } from "angular2-jwt";


@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.css']
})
export class NavbarAdminComponent implements OnInit {
  
  UserName :string;
  
  constructor(private jwtHelper: JwtHelper, private authGuard: AuthGuardService) { }

  ngOnInit() {
    this.GetUserNameFromLogin();
  }

  GetUserNameFromLogin(){

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);    
    this.UserName = tokenDecode.actort; 

    return this.UserName;
  }

  Logout(){
    localStorage.removeItem('jwt');
    this.authGuard.canActivate();
  }
}
