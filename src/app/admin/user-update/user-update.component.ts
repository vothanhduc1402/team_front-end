import  Swal  from 'sweetalert2';
import { JwtHelper } from 'angular2-jwt';
import { Component, OnInit,Inject, ViewChild } from '@angular/core';
import { User } from 'app/models/user';
import { MAT_DIALOG_DATA, MatDialogRef, MatOption } from '@angular/material';
import { UserService } from 'app/services/user.service';
import { Building } from 'app/models/building';
import {HttpClient, HttpHeaders,  } from '@angular/common/http';
import { Floor } from 'app/models/floor';
import { BuildingService } from 'app/services/building.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  @ViewChild('allSeclected') private allSeclected: MatOption;
  selectAllFloor : FormGroup;
  listFloor =new Building() ;
  getFloors = new Building();
  levels: string[] = ['Admin', 'FloorManager', 'Customer'];
  arrayBuilding : Building[];
  getFloorByBuilding = new Building();
  arrayFloor : Floor[];
  nameFormControl = new FormControl('',[Validators.required,Validators.minLength(6),Validators.maxLength(30)]);
  passwordFormControl = new FormControl('',[Validators.required,Validators.minLength(6),Validators.maxLength(30)]);
  phoneFormControl = new FormControl('',[Validators.required]);
  birthdayFormControl = new FormControl('',[Validators.required]);
  emailFormControl = new FormControl('',[Validators.required,Validators.email]);
  UserName: string;

  user: User;

  currentDate = new Date();
  year = this.currentDate.getFullYear();
  month = this.currentDate.getMonth();
  day = this.currentDate.getDay()
  myDate = new Date(this.currentDate);

  constructor(private dialogRef:  MatDialogRef<UserUpdateComponent>,
             @Inject(MAT_DIALOG_DATA) data:any,
             private userService: UserService,
             private http: HttpClient, 
             private buildingService: BuildingService, 
             private jwtHelper: JwtHelper,
             private fb: FormBuilder) { this.user=data;}

  ngOnInit() {
    this.getNameAllBuildingFloor();
    this.getBuildingFloorCurrent();
    this.selectChangeHandler(this.user.level);
    this.GetUserNameFromLogin();
    this.selectAllFloor = this.fb.group({
      floorType : new FormControl('')
    });
    this.myDate.setFullYear(this.year - 18);
    this.myDate.setMonth(this.month + 1);
    this.myDate.setDate(this.day);
  }

  getErrorMessageName(){
    return this.nameFormControl.hasError('required') ? 'Name is required!' :
      this.nameFormControl.hasError('minLength') || this.nameFormControl.hasError('maxLength')? '' : 'The name must contain between 6 and 30 characters'
  }

  getErrorMessagePassword(){
    return this.passwordFormControl.hasError('required') ? 'Password is required!' :
      this.passwordFormControl.hasError('minLength') || this.passwordFormControl.hasError('maxLength')? '' : 'The password must contain between 6 and 30 characters'
  }

  getErrorMessageEmail(){
    return this.emailFormControl.hasError('required') ? 'Email is required!' :
      this.emailFormControl.hasError('email') ? 'You must enter email like abc@gmail.com': ''
  }

  GetUserNameFromLogin(){
    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);    
    this.UserName = tokenDecode.actort; 
    return this.UserName;
  }

  getBuildingFloorCurrent(){
    if(this.user.id_building != null && this.user.id_building != ""){
      this.buildingService.getAllFloorByIDBuilding(this.user.id_building).subscribe(
        (data: Floor[]) => {
          console.log(`All floor = ${JSON.stringify(data)}`);
          this.arrayFloor = data;          
        }
      )
    }
  }
  getNameAllBuildingFloor(){
    this.buildingService.getAllBuilding().subscribe(
      (data: Building[]) => {
        this.arrayBuilding = data;
        console.log(`All building = ${JSON.stringify(data)}`);        
      }
    )
  }
  onSelectBuilding(idBuilding : string){
      console.log(`ID_Building = ${JSON.stringify(idBuilding)}`);
      this.buildingService.getAllFloorByIDBuilding(idBuilding).subscribe(
        (data: Floor[]) => {
          console.log(`All floor = ${JSON.stringify(data)}`);
          this.arrayFloor = data;          
        }
      )

  }
  onSelectFloors(idFloors : any){
    console.log(`ID_Floors = ${JSON.stringify(idFloors)}`);
  }

  public selectedValue: string;
  setDis:boolean;
  selectChangeHandler(level: string){
    this.selectedValue = level;
    if(this.selectedValue === "FloorManager"){
      this.setDis = false;
      
    }  
    else{
      this.setDis = true;
    }
      
  }

  getFloor(id_building : string){
    console.log(`id_building = ${JSON.stringify(id_building)}`);
    this.userService.getNameAllFloor(id_building).subscribe(building => {this.getFloors = building});
    this.listFloor = this.getFloors[0];
    console.log(`id_building = ${JSON.stringify(this.listFloor)}`);
  }

  onUpdate(user_new: User,password:string, name:string,
    email:string, phone:string, birthday:Date,level: string, id_building: string, id_floors : string[]=[]){
    user_new.password = password;
    user_new.name = name.trim();
    user_new.email = email.trim();
    user_new.phone = phone.trim();
    user_new.birthday = birthday;
    user_new.level = level;
    if(user_new.level == "Admin" || user_new.level == "Customer"){
      user_new.id_building = "";
      user_new.id_floors = [];
      user_new.person_update = this.UserName;
    }
    else{
      user_new.id_building = id_building;
      user_new.id_floors = this.selectAllFloor.controls.floorType.value
      user_new.person_create = this.UserName;
    }
    this.userService.updateUser(this.user).subscribe( _ => {
      this.onClose(); 
      Swal.fire({
        type: 'success',
        title: 'Your work has been saved',
        showConfirmButton: false,
        timer: 1500
      });
    });
  }

  onClose(){
    this.dialogRef.close();
  }

  toggleAllSelection(){
    if(this.allSeclected.selected){
      this.selectAllFloor.controls.floorType.patchValue([...this.arrayFloor.map(item => item.id_floor), 0]);
    }
    else{
      this.selectAllFloor.controls.floorType.patchValue([]);
    }
  }

  tosslePerOne(all){
    if(this.allSeclected.selected){
      this.allSeclected.deselect();
      return false;
    }
    if(this.selectAllFloor.controls.floorType.value.length==this.arrayFloor.length)
      this.allSeclected.select();
  }
}
