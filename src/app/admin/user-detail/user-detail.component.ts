import { Component, OnInit, Inject } from '@angular/core';
import { User } from 'app/models/user';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Building } from 'app/models/building';
import { Floor } from 'app/models/floor';
import { UserService } from 'app/services/user.service';
import { FormControl } from '@angular/forms';
import { BuildingService } from 'app/services/building.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  Formfloor = new FormControl();
  user: User;
  building = new Building() ;
  floor : Floor[];
  arrayFake : string[];
  arrNameFloor = new Array();
  constructor(private dialogRef: MatDialogRef<UserDetailComponent>,
              @Inject(MAT_DIALOG_DATA) data:any, 
              private userService: UserService ,
              private buildingService: BuildingService) { 
    this.user = data;
  }

  ngOnInit() {
    this.getAllBuildingFromId();
  }
  getAllBuildingFromId(){
    this.userService.getBuildingfromId(this.user.id).subscribe(
      (data : Building)=>{
        this.building = data;
        this.floor = this.building.floor ;

        for (let item of this.floor) {
          this.arrNameFloor.push(item.name_floor.toString());
        }  
        this.arrayFake = this.arrNameFloor;
              
      }
    );
  }

  onClose(){
    this.dialogRef.close();
  }
}
