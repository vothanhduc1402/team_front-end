import {
  Component,
  NgModule,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  Inject,
  Input
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject, of } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {BookingService} from '../../services/booking.service';
import {CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView} from 'angular-calendar';
import { Schedule } from 'app/models/schedule';
import { Router, ActivatedRoute} from '@angular/router';
import { Building } from './../../models/building';
import  {BuildingService} from '../../services/building.service';

import { Floor } from 'app/models/floor';
import { Room } from 'app/models/room';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef,MatDialogConfig, DateAdapter,MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NgbModalBackdrop } from '@ng-bootstrap/ng-bootstrap/modal/modal-backdrop';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import  Swal  from 'sweetalert2';




import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {defaultFormat as _rollupMoment} from 'moment';
import { JwtHelper } from 'angular2-jwt';
import { Deal } from 'app/models/deal';
import { tap, catchError } from 'rxjs/operators';
import { ROUTER_PROVIDERS } from '@angular/router/src/router_module';



const moment = _rollupMoment || _moment;


export const DD_MM_YYYY_Format = {
  parse: {
      dateInput: 'LL',
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-add-schedule-calendar',
  templateUrl: './add-schedule-calendar.component.html',
  styleUrls: ['./add-schedule-calendar.component.css'],
  
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddScheduleCalendarComponent implements OnInit {

  


  arrayBuilding : Building[];
  arrayFloor : Floor[];
  arrayRoom : Room[];
  arrayHour : number[] = [];
  arrayHourSchedule: number[] = [];
  arrayIDBuilding: string[] = [];
  id_Building: string;
  id_Floor: string;
  id_Room: string;
  id_user: string;
  user_name: string;
  listSchedule: any[] = [];
  dealCurrent : Deal[] = [];
  id_deal: string = "";
  dealExact : Deal;
  messagesAddSchedule: string;

  constructor(private router: Router, private bookingService: BookingService,private jwtHelper:JwtHelper ,private dialog: MatDialog, private dialogRef: MatDialogRef<AddScheduleCalendarComponent>, @Inject(MAT_DIALOG_DATA) data) {
      this.arrayIDBuilding = data;
      this.id_Building = this.arrayIDBuilding[0];
      this.id_Floor = this.arrayIDBuilding[1];
      this.id_Room = this.arrayIDBuilding[2];

  }




  

  flightSchedule = {
    date: new Date()
  }


  ngOnInit() {
    this.setHour();
    this.setHourSchedule();
    this.GetIdUserFromLogin()
    this.GetUserNameFromLogin();
    
  }

  GetIdUserFromLogin(){

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);
    this.id_user = tokenDecode.unique_name;

    return this.id_user;
  }

  GetUserNameFromLogin(){

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);
    this.user_name = tokenDecode.actort;

    return this.user_name;
  }

  settings = {
    bigBanner: true,
    timePicker: true,
    format: 'dd-MM-yyyy hh:mm a',
    defaultOpen: false,
    closeOnSelect: false,
  };

  setHour(){
    for(let i = 6; i < 24; i++){
      this.arrayHour.push(i);
    }
  }

  setHourSchedule(){
    for(let i = 1; i < 6; i++){
      this.arrayHourSchedule.push(i);
    }
  }

  onSelectHour(hour:number){

  }
  countBooking = 0;
  onSave(dateChoosed?: Date, hourChoose?: number, minuteChoose?: string, timeSchedule?: number){
   
   this.messagesAddSchedule = null;
    var minute = Number(minuteChoose);
    var dateStart = new Date(dateChoosed);
     // dateChoosed.setHours(15);
     dateStart.setHours(hourChoose);
      dateStart.setMinutes(minute);
      
      var dateFinish = new Date(dateStart);
      dateFinish.setHours(hourChoose+timeSchedule);
      // console.log(dateStart);
      // console.log(dateFinish);
      // console.log(this.id_Building);
      // console.log(this.id_Floor);
      // console.log(this.id_Room);
      // console.log(this.id_user);

      var schedules : Schedule[]  = [];
      var schedule = new Schedule();
      schedule.id_building = this.id_Building;
      schedule.id_floor = this.id_Floor;
      schedule.id_room = this.id_Room;
      schedule.time_start = dateStart;
      schedule.time_finish = dateFinish;
      schedule.date = dateChoosed;
      schedule.id_personapprove = "";
      schedule.issue = "";
      schedule.person_create = this.user_name;
      schedule.person_update = this.user_name;

      var deal = new Deal();
      deal.id_user = this.id_user;
      deal.person_create = this.user_name;
      deal.person_update = this.user_name;
      deal.schedule = this.listSchedule;
      deal.status = "closed";
    
      if(hourChoose == null || minuteChoose == null || timeSchedule == null){
        alert("Please enter full information")
      }
      else{

      
      

      schedules.push(schedule);


      this.bookingService.createDealSchedule(deal).subscribe(insertedDeal => {
        this.dealCurrent.push(insertedDeal) ; 
        this.dealExact = this.dealCurrent[0];
        this.id_deal = this.dealExact.id;
        
        this.bookingService.createSchedule(this.id_deal, schedules).pipe(
          tap((schedule: Schedule)=> console.log(`schedule: ${JSON.stringify(schedule)}`),
          (err: any) => this.messagesAddSchedule = err.error.message),
          catchError(error => of(null))
          )
        
         
          .subscribe( _ => {
            
           if(this.messagesAddSchedule === null){
             //this.router.navigateByUrl('/admin/Schedule-Calendar', { skipLocationChange: true });
            
            // location.reload();
              
           
              if (confirm('You have successfully registered. Do you want to continue booking?')) {
                // Save it!
                this.countBooking += 1;
               } else {
                this.onClose();
                location.reload();
            }
            //  Swal.fire({
            //   type: 'success',
            //   title: 'Create schedule success',
            //   showConfirmButton: false,
            //   timer: 500
            // });
           }
           else{
            alert(this.messagesAddSchedule);
           }
          }
          
      
        );
        
      },
   
      
      )
      
      
      this.dealCurrent = [];
      
  }
}
  onClose(){
    this.dialog.closeAll();
  }
  onCloseMain(){
    if(this.countBooking > 0){
      this.dialog.closeAll();
      location.reload();
    }
    else{
      this.dialog.closeAll();
    }
  }

}
