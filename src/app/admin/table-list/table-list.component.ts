


import { UserService } from './../../services/user.service';
import { User } from './../../models/user';
import { Component, OnInit, Input,  } from '@angular/core';
import { FormControl,FormGroup, FormBuilder } from '@angular/forms';
import {EMPTY} from 'rxjs';
import { filter,switchMap, debounceTime, catchError } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { AddUserComponent } from '../add-user/add-user.component';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { UserUpdateComponent } from '../user-update/user-update.component';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

  findControl = new FormControl();
  user : User = null;
  error: boolean = false;
  lists: User[] = [];
  listsSearch: User[] = [];
  arr: string[];

  constructor(private userService: UserService,
              private dialog: MatDialog) { }

  userinfor: User;
  
  openDialogUserDetail(userinfo: User){
    const dialogConfig = new MatDialogConfig();
        this.userinfor = userinfo;
        console.log(`select user = ${JSON.stringify(this.userinfor)}`);
        dialogConfig.data = this.userinfor;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "40%";
        this.dialog.open(UserDetailComponent, dialogConfig);
  }

  openDialogUserUpdate(userinfo: User){
    const dialogConfig = new MatDialogConfig();
        this.userinfor = userinfo;
        console.log(`select user = ${JSON.stringify(this.userinfor)}`);
        dialogConfig.data = this.userinfor;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "50%";
        this.dialog.open(UserUpdateComponent, dialogConfig);
  }

  getUserFromService(): void{
    this.userService.getUser().subscribe(updatedUser => this.lists = updatedUser);
  }
  ngOnInit() {
    this.getUserFromService();
    this.findControl.valueChanges
      .pipe(
        // Filter if less than two characters are entered
        filter (value => value.length>= 0),
        
        // Set the delay to one second
        debounceTime (100),

        // Requesting user data
        
        switchMap (value =>
          this.userService.searchUser (value) .pipe (
            // Error processing
            catchError (err => {
              this.user = null;
              this.error = true;
              return EMPTY;
            })
          )
        )
      )
      // Get the data
      .subscribe (user => {
       this.lists = null;
       this.user = user;        
        this.error = false;
      });
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.maxHeight = "100%";
    this.dialog.open(AddUserComponent, dialogConfig);
}

  delete(userId: string): void {
    this.userService.deleteUser(userId).subscribe(_ => {
      this.lists = this.lists.filter(eachUser => eachUser.id !== userId);
    });
  };

  alertFunction(userId: string){
    // let userId : string;
    // if(confirm('Are you sure to delete this user?')){
    //   return this.delete(userId);
    // }
    // else{
    //   return false;
    // }

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

      if (result.value) {
        this.delete(userId);
        Swal.fire(
          'Deleted!',
          'Your building has been deleted.',
          'success'
        )
      }
    })
  }
}
