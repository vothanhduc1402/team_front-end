import { Component, OnInit } from '@angular/core';
import { Room } from 'app/models/room';
import { BuildingService } from 'app/services/building.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MngRoomsAddComponent } from './mng-rooms-add/mng-rooms-add.component';
import { MngRoomsUpdateComponent } from './mng-rooms-update/mng-rooms-update.component';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-mng-rooms',
  templateUrl: './mng-rooms.component.html',
  styleUrls: ['./mng-rooms.component.css']
})
export class MngRoomsComponent implements OnInit {

  idbuilding: string;
  idfloor: string;
  listRoom : Room[];
  arrayId :string[]=[];
  arrayData :any[]=[];
  constructor(private buildingService: BuildingService,
              private ActivatedRoute: ActivatedRoute, 
              private router: Router,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.idbuilding = this.ActivatedRoute.snapshot.params['id'];
    this.idfloor = this.ActivatedRoute.snapshot.params['id_floor'];
    this.getAllRoomByIdFloor(this.idbuilding,this.idfloor);
  }

  getAllRoomByIdFloor(idBuilding: string, idFloor: string){
    this.buildingService.getAllRoomByIDFloor(idBuilding,idFloor).subscribe(
      (data: Room[]) => {
        this.listRoom = data;
        console.log(`All Room = ${JSON.stringify(data)}`);        
      }
    )    
  }

  AddRoom(){
    this.arrayId = [];
    this.arrayId.push(this.idbuilding);
    this.arrayId.push(this.idfloor);
    console.log(this.arrayId);
    
    const dialogconfig = new MatDialogConfig();
    dialogconfig.disableClose = true;
    dialogconfig.autoFocus = true;
    dialogconfig.data = this.arrayId;
    this.dialog.open(MngRoomsAddComponent, dialogconfig);
  }

  UpdateRoom(room: Room){
    this.arrayData = [];
    this.arrayData.push(this.idbuilding);
    this.arrayData.push(this.idfloor);
    this.arrayData.push(room);
    console.log(this.arrayData);
    const dialogconfig = new MatDialogConfig();
    dialogconfig.disableClose = true;
    dialogconfig.autoFocus = false;
    dialogconfig.data = this.arrayData;
    this.dialog.open(MngRoomsUpdateComponent, dialogconfig);
  }
  DeleteRoom(idroom: string){
    console.log(idroom);
    this.buildingService.deleteRoom(this.idbuilding,this.idfloor, idroom)
    .subscribe(_ => {
      this.listRoom = this.listRoom.filter(eachRoom => eachRoom.id_room !== idroom);
    });   
  }

  alertFunction(idRoom: string){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

      if (result.value) {
        this.DeleteRoom(idRoom);
        Swal.fire(
          'Deleted!',
          'Your building has been deleted.',
          'success'
        )
      }
    })
  }
}

