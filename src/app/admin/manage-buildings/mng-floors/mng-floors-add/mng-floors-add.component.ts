import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Floor } from 'app/models/floor';
import { BuildingService } from 'app/services/building.service';
import { Router, ActivatedRoute } from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { GetUserNameService } from 'app/shared/get-user-name.service';
import {FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import  Swal  from 'sweetalert2';
import { Observable,of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';

@Component({
  selector: 'app-mng-floors-add',
  templateUrl: './mng-floors-add.component.html',
  styleUrls: ['./mng-floors-add.component.css']
})
export class MngFloorsAddComponent implements OnInit {
 
  idbuilding : string;
  listFloor: Floor[] = [];
  namefloor = new FormControl('',[Validators.required]);
  floorformGroup: FormGroup;
  messageFromService: string = null;

  constructor(private dialog: MatDialog, 
              private buildingService : BuildingService,
              private ActivatedRoute: ActivatedRoute, 
              private router: Router, 
              private dialogRef: MatDialogRef<MngFloorsAddComponent>,
              @Inject(MAT_DIALOG_DATA) data, 
              private getUserNameService:GetUserNameService,
              private formBuilder: FormBuilder) {
      
      this.idbuilding = data }

  ngOnInit() {
    this.createForm();
  }
  createForm(){
    this.floorformGroup = this.formBuilder.group({
      namefloor : ['',[Validators.required]],
    });
  }
  onSave(name_floor : string){
    this.messageFromService = null;
    var new_floor: Floor = new Floor();
    new_floor.name_floor = name_floor;
    new_floor.total_room = "0";
    new_floor.room = [];
    new_floor.person_create = this.getUserNameService.GetUserNameFromLogin();
    new_floor.time_create = new Date();
    this.buildingService.addFloor(this.idbuilding, new_floor).pipe(
      tap((floor: Floor) => console.log(`inserted floor = ${JSON.stringify(floor)}`),
      (err) => this.messageFromService = err.error.message),
      catchError(error => of(new Floor()))
    ).subscribe(insertFloor => {
      this.listFloor.push(insertFloor);
      if(this.messageFromService === null){
        this.onClose();
        Swal.fire({
          type: 'success',
          title: 'Your work has been saved',
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigate([`/admin/manage-buildings/manage-floors/${this.idbuilding}`]);
      }
      
    });   
  }

  onClose(){
    this.dialog.closeAll();
  }
}
