import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { BuildingService } from 'app/services/building.service';
import { Floor } from 'app/models/floor';
import { GetUserNameService } from 'app/shared/get-user-name.service';
import {FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { catchError, tap, } from 'rxjs/operators';
import { of } from 'rxjs';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-mng-floors-update',
  templateUrl: './mng-floors-update.component.html',
  styleUrls: ['./mng-floors-update.component.css']
})
export class MngFloorsUpdateComponent implements OnInit {
 
  arrayData : any[]=[];
  floor = new Floor();
  idBuilding : string;
  namefloor = new FormControl('',[Validators.required]);
  floorformGroup: FormGroup;
  messageFromService: string = null;

  constructor(private dialog: MatDialog, private buildingService : BuildingService, private dialogRef: MatDialogRef<MngFloorsUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) data, private getUserNameService: GetUserNameService,private formBuilder: FormBuilder) {
      this.arrayData = data; 
      this.floor = this.arrayData[1] ;
      this.idBuilding = this.arrayData[0];
    }

  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.floorformGroup = this.formBuilder.group({
      namefloor : ['',[Validators.required]],
    });
  }
  
  onClose(){
    this.dialog.closeAll();
  }

  onUpdate(name_floor : string){ 
    this.messageFromService = null;
    console.log(this.arrayData);  
    this.floor.name_floor = name_floor;
    this.floor.preson_update = this.getUserNameService.GetUserNameFromLogin();
    this.floor.time_update = new Date();
    this.buildingService.updateFloor(this.idBuilding, this.floor.id_floor, this.floor).pipe(
      tap(updateFloor => console.log(`update floor = ${JSON.stringify(updateFloor)}`),
      (err) => this.messageFromService = err.error.message),
      catchError(error => of(new Floor()))
      ).subscribe(() => {
        if(this.messageFromService === null){
          this.onClose();
          Swal.fire({
            type: 'success',
            title: 'Your work has been saved',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
  }
}
