import { Component, OnInit } from '@angular/core';
import { BuildingService } from './../../../services/building.service';
import { Floor} from './../../../models/floor';
import { Building } from 'app/models/building';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MngFloorsAddComponent } from './mng-floors-add/mng-floors-add.component';
import { MngFloorsUpdateComponent } from './mng-floors-update/mng-floors-update.component';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-mng-floors',
  templateUrl: './mng-floors.component.html',
  styleUrls: ['./mng-floors.component.css']
})
export class MngFloorsComponent implements OnInit {

  idbuilding : string;
  listFloor : Floor[]; 
  arrayData: any[]=[];

  constructor(private buildingService: BuildingService,
              private ActivatedRoute: ActivatedRoute, 
              private router: Router,
              private dialog: MatDialog ) { }

  ngOnInit() {
    this.idbuilding = this.ActivatedRoute.snapshot.params['id'];
    this.getAllFloorByIdBuilding(this.idbuilding);
  }

  getAllFloorByIdBuilding(idBuilding: string){
    this.buildingService.getAllFloorByIDBuilding(idBuilding).subscribe(
      (data: Floor[]) => {
        this.listFloor = data;
        console.log(`All floor = ${JSON.stringify(data)}`);        
      }
    )    
  }

  AddFloor(){
    const dialogconfig = new MatDialogConfig();
    dialogconfig.disableClose = true;
    dialogconfig.autoFocus = true;
    dialogconfig.data = this.idbuilding;
    this.dialog.open(MngFloorsAddComponent, dialogconfig);
  }

  UpdateFloor(floor: Floor){
    this.arrayData = [];
    this.arrayData.push(this.idbuilding);
    this.arrayData.push(floor);
    console.log(this.arrayData);
    
    const dialogconfig = new MatDialogConfig();
    var id = this.idbuilding;
    dialogconfig.disableClose = true;
    dialogconfig.autoFocus = false;
    dialogconfig.data = this.arrayData;
    this.dialog.open(MngFloorsUpdateComponent, dialogconfig);
  }

  DeleteFloor(idfloor: string){
    console.log(idfloor);
    this.buildingService.deleteFloor(this.idbuilding,idfloor).subscribe(_ => {
      this.listFloor = this.listFloor.filter(eachFloor => eachFloor.id_floor !== idfloor);
    });   
  }

  alertFunction(idFloor: string){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

      if (result.value) {
        this.DeleteFloor(idFloor);
        Swal.fire(
          'Deleted!',
          'Your building has been deleted.',
          'success'
        )
      }
    })
  }
}
