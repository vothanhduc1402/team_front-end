import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MatDialog } from "@angular/material";
import { Building } from 'app/models/building';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BuildingService } from 'app/services/building.service';
import { GetUserNameService } from 'app/shared/get-user-name.service';
import  Swal  from 'sweetalert2';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-mng-building-update',
  templateUrl: './mng-building-update.component.html',
  styleUrls: ['./mng-building-update.component.css']
})
export class MngBuildingUpdateComponent implements OnInit {

  building: Building;
  UserName: string;
  messageFromServiceUpdate: string = null;
  constructor(private dialog: MatDialog, 
              private buildingService: BuildingService, 
              private getUserNameService: GetUserNameService,
              private dialogRef:  MatDialogRef<MngBuildingUpdateComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) protected data:any) {
                this.building = data;
               }

  ngOnInit() {
    this.UserName = this.getUserNameService.GetUserNameFromLogin();
  }

  OnClose(){
    this.dialog.closeAll();
  }

  OnUpdate(building_new: Building, name_building: string){
    this.messageFromServiceUpdate = null;
    building_new.name_building = name_building;
    building_new.person_update = this.UserName;
    
    this.buildingService.updateBuilding(this.building).pipe(
      tap(  (building: Building) => console.log(`update floor = ${JSON.stringify(building)}`),
            err => this.messageFromServiceUpdate = err.error.message
         ), 
          catchError(error => of(new Building()))).
          subscribe(_ => {
            if(this.messageFromServiceUpdate === null){
              this.OnClose(); 
              Swal.fire({
                type: 'success',
                title: 'Your work has been saved',
                showConfirmButton: false,
                timer: 1500
              });
            } 
          });
  }
}
