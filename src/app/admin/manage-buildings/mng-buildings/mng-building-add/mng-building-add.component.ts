import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog,MatDialogRef } from "@angular/material";
import { Building } from 'app/models/building';
import { BuildingService } from 'app/services/building.service';
import { GetUserNameService } from 'app/shared/get-user-name.service';
import { catchError, tap, } from 'rxjs/operators';
import { of } from 'rxjs';
import  Swal  from 'sweetalert2';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-mng-building-add',
  templateUrl: './mng-building-add.component.html',
  styleUrls: ['./mng-building-add.component.css']
})
export class MngBuildingAddComponent implements OnInit {
  listBuilding: Building[] = [];

  public messageFromService: string = null;
  UserName: string;
  nameBuiildingFormControl = new FormControl('',[Validators.required]);
  buildingFormGroup: FormGroup;

  constructor(private dialog: MatDialog, 
              private buildingService: BuildingService, 
              private getUserNameService: GetUserNameService,
              private location: Location,
              private router : Router,
              private dialogref: MatDialogRef <MngBuildingAddComponent>,
              private formBuilder: FormBuilder) { this.createForm();}

  ngOnInit() { 
     this.UserName = this.getUserNameService.GetUserNameFromLogin();
    //  this.messageFromService = this.buildingService.messages;
     
  }
  createForm(){
    this.buildingFormGroup = this.formBuilder.group({
      nameBuiildingFormControl : ['',[Validators.required]],
    });
  }
  OnClose() {
    this.dialogref.close();
   }

  OnSave(NameBuilding: string) { 
    this.messageFromService = null;

    var newBuilding = new Building();
    
    newBuilding.name_building = NameBuilding;
    newBuilding.total_floor = "0";
    newBuilding.floor = [];
    newBuilding.person_create = this.UserName;
    this.buildingService.addBuilding(newBuilding).
    pipe(
    tap((building: Building) => console.log(`inserted Building = ${JSON.stringify(building)}`),
        (err) => this.messageFromService = err.error.message),
        catchError(error => of(new Building()))
      ).
      subscribe( insertedbuilding =>  {
        this.listBuilding.push(insertedbuilding);
        if(this.messageFromService === null){
          this.OnClose(); 
          Swal.fire({
            type: 'success',
            title: 'Your work has been saved',
            showConfirmButton: false,
            timer: 2500
          });
          this.router.navigateByUrl('/admin/manage-buildings');
        }   
      })
  }
}
