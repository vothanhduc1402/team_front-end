
// import { MngBuildingAddComponent } from './mng-building-add/mng-building-add.component';
import { BuildingService } from './../../../services/building.service';
import { Building } from './../../../models/building';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MngBuildingUpdateComponent } from './mng-building-update/mng-building-update.component';
import { MngBuildingAddComponent } from './mng-building-add/mng-building-add.component';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-mng-buildings',
  templateUrl: './mng-buildings.component.html',
  styleUrls: ['./mng-buildings.component.css']
})
export class MngBuildingsComponent implements OnInit {

  listBuilding : Building[];
  buildingInfor : Building;

  constructor(private buildingService: BuildingService,  private dialog: MatDialog) { }

  ngOnInit() {
    this.GetAllBuidingService();
  }

  GetAllBuidingService(){
    this.buildingService.getAllBuilding().subscribe(
      (data:Building[]) => {
        this.listBuilding = data;
      }
    )
  }

  DeleteBuilding(BuildingId: string): void {
    this.buildingService.deleteBuilding(BuildingId).subscribe(_ => {
      this.listBuilding = this.listBuilding.filter(eachBuilding => eachBuilding.id !== BuildingId);
    });
  };

  alertFunction(buildingId: string){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

      if (result.value) {
        this.DeleteBuilding(buildingId);
        Swal.fire(
          'Deleted!',
          'Your building has been deleted.',
          'success'
        )
      }
    })
  }

  OpenDialogAddBuilding(buildingInfo: Building){
    const dialogConfig = new MatDialogConfig();
    this.buildingInfor = buildingInfo;
    dialogConfig.data = this.buildingInfor;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "40%";
    this.dialog.open(MngBuildingAddComponent, dialogConfig);
  }

  OpenDialogUpdateBuilding(buildingInfo: Building){
    const dialogConfig = new MatDialogConfig();
    this.buildingInfor = buildingInfo; 
    dialogConfig.data = this.buildingInfor;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "40%";
    this.dialog.open(MngBuildingUpdateComponent, dialogConfig);
  }
}
