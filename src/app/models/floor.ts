import { Room } from "./room";

export class Floor{
    id_floor: string;
    name_floor: string;
    room: Room[];
    total_room: string;
    person_create: string;
    preson_update: string;
    time_update: Date;
    time_create: Date;
} 