export class Room{
    id_room: string;
    name_room: string;
    size: string;
    person_create: string;
    person_update: string;
    time_update: Date;
    time_create: Date;
}