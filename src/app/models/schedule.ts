export class Schedule{
    id_schedule: string;
    id_building: string;
    id_floor: string;
    id_room: string;
    time_start: Date;
    time_finish: Date;
    date: Date;
    status: string;
    id_personapprove: string;
    issue: string;
    person_create: string;
    person_update: string;
    time_update: Date;
    time_create: Date;
}