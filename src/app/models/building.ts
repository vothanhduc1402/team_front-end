import { Floor } from "./floor";

export class Building{
    id: string;
    name_building: string;
    floor: Floor[];
    total_floor: string;
    person_create: string;
    person_update: string;
    time_update: Date;
    time_create: Date;
}