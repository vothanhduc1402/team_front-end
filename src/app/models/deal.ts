import { Schedule } from "./schedule";

export class Deal{
    id: string;
    id_user: string;
    status: string;
    schedule: Schedule[] = [];
    person_create: string;
    person_update: string;
    time_update: Date;
    time_create: Date;
}