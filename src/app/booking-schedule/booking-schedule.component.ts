import {
  Component,
  NgModule,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  ChangeDetectorRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  setHours
} from 'date-fns';
import { Subject, of, Observable, concat, forkJoin } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { BookingFloormanagerService } from '../services/booking-floormanager.service';
import { BookingService } from '../services/booking.service';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, CalendarWeekViewAllDayEvent, CalendarEventTitleFormatter } from 'angular-calendar';
import { Schedule } from 'app/models/schedule';
import { Router, ActivatedRoute } from '@angular/router';
import { Building } from '../models/building';
import { BuildingService } from '../services/building.service';
import { UserService } from '../services/user.service';
import { Floor } from 'app/models/floor';
import { Room } from 'app/models/room';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NgbModalBackdrop } from '@ng-bootstrap/ng-bootstrap/modal/modal-backdrop';
import Swal from 'sweetalert2';
import { tap, catchError } from 'rxjs/operators';
import { ObserveOnSubscriber } from 'rxjs/internal/operators/observeOn';
import { Deal } from 'app/models/deal';
import { JwtHelper } from 'angular2-jwt';
import { BookingCustomerService } from 'app/services/booking-customer.service';





const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  gray: {
    primary: '#000000',
    secondary: '#C6CACE'
  },
  green: {
    primary: '#1d6318',
    secondary: '#93f48d'
  }

};
@Component({
  selector: 'app-booking-schedule',
  templateUrl: './booking-schedule.component.html',
  styleUrls: ['./booking-schedule.component.css']
})
export class BookingScheduleComponent implements OnInit {

  id_building: string;
  id_floor: string;
  id_room: string = "";

  id_building_schedule: string;

  idBuilding: string = null;
  idFloor: string = null;
  idRoom: string = null;
  arrayBuilding: Building[];
  arrayFloor: Floor[] = [];
  arrayRoom: Room[];
  openButton: boolean;
  arrayIDbuilding: string[] = [];
  calendar: CalendarWeekViewAllDayEvent;
  dayCurrent = new Date();
  nrSelect = 0;
  hourStart: number;
  hourEnd: number;
  minuteStart: number;
  rentHour: number;
  arrayHour: number[] = [];
  arrayRentHour: number[] = [];
  modalReference = null;
  modalAddReference = null;
  checkTimeLesserCurrent = null;
  selectedEvent: CalendarEvent;
  selectedTimeStart: Date;
  selectedTimeEnd: Date;
  scheduleUpdate = new Schedule();
  id_user: string;
  user_name: string;
  action: string = "";
  managementBuildingFloors: Building;
  role: string;
  listScheduleFromIdUser: Schedule[];

  constructor(private dialog: MatDialog, private modal: NgbModal, private BookingService: BookingService,
    private BuildingService: BuildingService, private BookingFloormanagerService: BookingFloormanagerService,
    private _ActivatedRoute: ActivatedRoute, private _router: Router, private jwtHelper: JwtHelper,
    private UserService: UserService, private BookingCustomerService: BookingCustomerService) {
  }


  ngOnInit() {
    this.GetIdUserFromLogin();
    this.GetUserNameFromLogin();
    this.GetRoleFromLogin()
    this.getAllBuilding();
    this.openButton = true;
    this.setHour();
    this.setRentHour();
    this.getScheduleFromIdUser();

  }

  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @ViewChild('modalAddSchedule') modalAddSchedule: TemplateRef<any>;


  view: CalendarView = CalendarView.Week;
  CalendarView = CalendarView;
  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        console.log(event);

        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            this.events = this.events.filter(iEvent => iEvent !== event);
            this.deleteEvent(event);
            this.refresh.next();
            console.log(this.listSchedule);

          }
        })

      }
    }
  ];

  refresh: Subject<any> = new Subject();
  listSchedule: Schedule[];
  listScheduleFromFloor: Schedule[];
  listScheduleFromFloorByWeek: Schedule[];
  listScheduleFromRoomByWeek: Schedule[];
  listNameRoom: Room[] = [];
  selected = 0;
  events: CalendarEvent[] = [];
  activeDayIsOpen: boolean = true;


  GetIdUserFromLogin() {

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);
    this.id_user = tokenDecode.unique_name;
    this.getBuildingFloorByIdUser(this.id_user);

    return this.id_user;
  }

  GetUserNameFromLogin() {

    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);
    this.user_name = tokenDecode.actort;

    return this.user_name;
  }
  GetRoleFromLogin() {
    let token = localStorage.getItem("jwt");
    let tokenDecode = this.jwtHelper.decodeToken(token);
    this.role = tokenDecode.role;
    return this.role;
  }
  getBuildingFloorByIdUser(idUser: string) {
    this.UserService.getNameBuildingFromIdUser(this.id_user).subscribe(
      (data: Building) => {
        console.log(data);
        this.managementBuildingFloors = data;
      }
    )
  }
  getAllBuilding() {
    this.BuildingService.getAllBuilding().subscribe(
      (data: Building[]) => {
        console.log(data);
        this.arrayBuilding = data;
      }
    )
  }

  onSelectBuilding(idBuilding: string) {
    this.arrayFloor = [];
    this.arrayRoom = [];
    this.selected = 0;
    this.BuildingService.getAllFloorByIDBuilding(idBuilding).subscribe(
      (data: Floor[]) => {

        this.arrayFloor = data;

      }
    )

    this.events = [

    ];
    this.openButton = true;
    this.id_building = idBuilding;
    this.idBuilding = idBuilding;
    this.idFloor = null;
    this.idRoom = null;
  }

  dateofweek: Date;
  onSelectFloor(idBuilding: string, idFloor: string) {
    this.events = [];
    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    //this.getNameRoom(idBuilding,idFloor);
    this.BuildingService.getAllRoomByIDFloor(idBuilding, idFloor).subscribe(
      (data: Room[]) => {
        console.log(data);
        this.arrayRoom = data;

      }
    );
    if (day1 < day2) {
      this.events = [];
      this.getScheduleFloorByWeek(idFloor, this.dateofweek.toISOString());
      this.id_floor = idFloor;
      this.id_room = null;
    }
    else {
      this.events = [];
      this.getAllScheduleFromFloor(idBuilding, idFloor, this.dateofweek.toISOString());
      this.dateofweek = new Date(this.viewDate);
      this.openButton = true;
      this.id_floor = idFloor;
      this.id_room = null;
      console.log(this.id_room);

      this.idFloor = idFloor;
      this.nrSelect = 0;
      this.idRoom = null;
    }


  }

  onSelectRoom(idRoom: string) {
    if (idRoom == "0") {
      this.onSelectFloor(this.idBuilding, this.idFloor);
    }
    else {
      this.openButton = false;
      this.events = [];
      this.getAllScheduleByRoom(idRoom);
      this.dateofweek = new Date(this.viewDate);
      this.getScheduleRoomByWeek(this.id_floor, idRoom, this.dateofweek.toISOString());
      this.id_room = idRoom;
      this.idRoom = idRoom;
    }


  }

  onWeekPrevious() {


    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    this.events = [];
    if (day1 == day2 && this.id_room == null) {
      this.onSelectFloor(this.id_building, this.id_floor);
    }
    else if (day1 == day2 && this.id_room != null) {
      this.events = [];
      this.onSelectRoom(this.id_room);
    }
    else if (day1 > day2 && this.id_room == null) {
      this.events = [];
      this.onSelectFloor(this.id_building, this.id_floor);
    }
    else if (day1 > day2 && this.id_room != null) {
      this.events = [];
      this.onSelectRoom(this.id_room);
    }
    else if (this.id_room != null) {
      this.events = [];
      this.getScheduleRoomByWeek(this.id_floor, this.id_room, this.dateofweek.toISOString())
    }
    else {
      this.events = [];
      this.getScheduleFloorByWeek(this.id_floor, this.dateofweek.toISOString());
    }

  }

  onWeekNext() {
    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    this.events = [];
    if (day1 == day2 && this.id_room == null) {
      this.events = [];
      this.onSelectFloor(this.id_building, this.id_floor);

    }
    else if (day1 == day2 && this.id_room != null) {
      this.events = [];
      this.onSelectRoom(this.id_room);
    }
    else if (day1 < day2 && this.id_room != null) {
      this.events = [];
      this.getScheduleRoomByWeek(this.id_floor, this.id_room, this.dateofweek.toISOString());

    }

    else if (day1 < day2 && this.id_room == null) {
      this.events = [];
      this.getScheduleFloorByWeek(this.id_floor, this.dateofweek.toISOString());

    }
    else if (day1 > day2 && this.id_room == null) {
      this.onSelectFloor(this.idBuilding, this.idFloor);
    }
    else if (day1 > day2 && this.id_room != null) {
      this.onSelectRoom(this.id_room);
    }

  }
  onToday() {
    this.dateofweek = new Date(this.viewDate);
    var day1 = this.dateofweek.getDate();
    var day2 = this.dayCurrent.getDate();
    this.events = [];
    if (day1 == day2 && this.id_room == null) {
      this.events = [];
      this.onSelectFloor(this.id_building, this.id_floor);
    }
    else if (this.id_room != null) {
      this.events = [];
      this.onSelectRoom(this.id_room);
    }


  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }
  eventClicked({ event }: { event: CalendarEvent }): void {
    console.log('Event clicked', event);
    this.action = "Click event";
    // if(event.id == "cd963bb3-6a06-47b8-8da8-18e4ba63b21a"){
    this.hourStart = Number(event.start.getHours());
    this.minuteStart = Number(event.start.getMinutes());
    this.hourEnd = Number(event.end.getHours());
    if (this.hourEnd == 0) {
      this.hourEnd = 24;
    }
    this.rentHour = Number(this.hourEnd - event.start.getHours());
    Promise.resolve(null).then(() => this.hourStart = Number(event.start.getHours()));
    Promise.resolve(null).then(() => this.minuteStart = Number(event.start.getMinutes()));
    this.selectedEvent = event;

    console.log(this.role);

    // check if belong building floors management then show modal update
    if (event.id != null && this.role == "Admin") {
      console.log("ccc");
      this.handleEvent("Click event", event);
    }
    
    
    else if (this.role == "FloorManager") {
      console.log("ccc");
      if (this.managementBuildingFloors.id == this.idBuilding && event.id != null) {
        var filter = this.managementBuildingFloors.floor.filter(x => x.id_floor == this.idFloor)[0];
        if (filter != null) {
          this.handleEvent("Click event", event);
        }
      }
    }
    
    else if (this.role == "Customer" ) {
      console.log("ccc");
      
      let filter = this.listScheduleFromIdUser.filter(sche => sche.id_schedule == event.id)[0];
      if( !filter){
        console.log("Not schedule of you");
      } 
      else {
        this.handleEvent("Click event", event);
      }      
      
    }
    
    //this.handleEvent("Click event", event);
    //}

  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      var dateNow = new Date();
      var newDateStart = newStart;
      var newDateEnd = newEnd;
      //backup event when close modal
      if (event.id == iEvent.id) {
        this.selectedEvent = iEvent;
        this.selectedTimeStart = iEvent.start;
        this.selectedTimeEnd = iEvent.end;
      }

      if (iEvent === event && newDateStart >= dateNow) {
        if (this.CheckScheduleForTimeStart(event.id.toString(), newDateStart, newDateEnd) == true ||
          this.CheckScheduleForTimeEnd(event.id.toString(), newDateStart, newDateEnd) == true ||
          this.CheckScheduleForTimeFull(event.id.toString(), newDateStart, newDateEnd) == true) {
          Swal.fire(
            'Time conflict',
            '',
            'warning'
          );
        }
        else if ((newDateEnd.getHours() - newDateStart.getHours()) > 5 ||
          (newDateEnd.getHours() - newDateStart.getHours()) <= -7 &&
          (newDateEnd.getHours() - newDateStart.getHours()) > -19) {
          Swal.fire(
            'Book room no more than 5 hours',
            '',
            'warning'
          )
        }
        else {
          console.log(event);

          event.start = newStart;
          event.end = newEnd;
          this.action = "Dropped or resized";
          this.handleEvent('Dropped or resized', event);
          return {
            ...event,
            id: event.id,
            start: newStart,
            end: newEnd,
            draggable: true
          };
        }
      }
      else if (iEvent === event && newDateStart < dateNow) {
        Swal.fire(
          'Not book before the current date',
          '',
          'warning'
        )
        return {
          ...event,
          id: event.id,
          start: iEvent.start,
          end: iEvent.end,
          draggable: true
        };
      }
      return iEvent;
    });
    // handle hour start, minute start and hour end for modal update schedule
    this.hourStart = Number(event.start.getHours());
    this.minuteStart = Number(event.start.getMinutes());
    this.hourEnd = Number(event.end.getHours());
    if (this.hourEnd == 0) {
      this.hourEnd = 24;
    }
    this.rentHour = Number(this.hourEnd - event.start.getHours());
    Promise.resolve(null).then(() => this.hourStart = Number(event.start.getHours()));
    Promise.resolve(null).then(() => this.minuteStart = Number(event.start.getMinutes()));

  }

  objtest: any;
  addEvent() {

    this.objtest = {
      title: 'New event 1',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      actions: this.actions,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    }
    this.events.push(this.objtest);
    this.refresh.next();
    console.log("event");
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    if (this.role == "Admin") {
      this.BookingService.updateStatusDeal(eventToDelete.id.toString(), "closed").subscribe(
        x => {
          this.modalReference.close();  
        }
      );
    }
    else if (this.role == "FloorManager") {
      this.BookingFloormanagerService.updateStatusDeal(eventToDelete.id.toString(), "closed").subscribe(
        x => {
          this.modalReference.close();  
        }
      );
      
    }
    else if (this.role == "Customer") {
      this.BookingCustomerService.deleteSchedule(eventToDelete.id.toString()).subscribe(
        x => {
          this.modalReference.close();  
        }
      );
    }

    // update listschedule
    var sche = this.listSchedule.filter(sche => sche.id_schedule == eventToDelete.id)[0];
    this.listSchedule.splice(this.listSchedule.indexOf(sche), 1);
    this.events = this.events.filter(event => event.id !== eventToDelete.id);
    this.refresh.next();
    
    
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }



  obj: any;
  getAllScheduleByRoom(id_roomSchedule: string) {
    this.BookingService.getScheduleFromIdRoom(id_roomSchedule).subscribe(
      (data: Schedule[]) => {

        this.listSchedule = data;
        for (var i = 0; i < this.listSchedule.length; i++) {
          this.obj = {
            id: this.listSchedule[i].id_schedule,
            start: new Date(this.listSchedule[i].time_start),
            end: new Date(this.listSchedule[i].time_finish),
            title: "<b>" + "R." + this.listNameRoom.filter(room => room.id_room == this.listSchedule[i].id_room)[0].name_room,
            color: this.setColors(this.listSchedule[i].status, this.listSchedule[i].id_schedule),
            //actions: this.actions,
            resizable: {
              beforeStart: this.checkManagementBuildingFloors(this.listSchedule[i]),
              afterEnd: this.checkManagementBuildingFloors(this.listSchedule[i])
            },
            //draggable: true,
            draggable: this.checkManagementBuildingFloors(this.listSchedule[i]),
          }
          this.events.push(this.obj);
        }
        this.refresh.next();
      }
    )

  }

  protected objScheduleOfFloor: any;
  getAllScheduleFromFloor(idBuilding: string, idFloor: string, dayofweek: string) {
    const combine = forkJoin(this.BookingService.getNameRoom(idBuilding, idFloor),
      this.BookingService.getScheduleFromFloor(idBuilding, idFloor),
      this.BookingService.getScheduleFromFloorByWeek(idFloor, dayofweek)).subscribe(
        ([res1, res2, res3]) => {
          this.listNameRoom = res1;
          this.listScheduleFromFloor = res2;
          this.listScheduleFromFloorByWeek = res3;
          for (var i = 0; i < this.listScheduleFromFloor.length; i++) {
            this.objScheduleOfFloor = {
              start: new Date(this.listScheduleFromFloor[i].time_start),
              end: new Date(this.listScheduleFromFloor[i].time_finish),
              title: "<b>" + "R." + + this.listNameRoom.filter(room => room.id_room == this.listScheduleFromFloor[i].id_room)[0].name_room + "</b>",
              color: this.setColors(this.listScheduleFromFloor[i].status, this.listScheduleFromFloor[i].id_schedule),
              allDay: false,
              resizable: {
                beforeStart: false,
                afterEnd: false
              },
              draggable: false
            }

            this.events.push(this.objScheduleOfFloor);

          }
          this.refresh.next();

          for (var i = 0; i < this.listScheduleFromFloorByWeek.length; i++) {
            this.objScheduleOfFloorByWeek = {
              start: new Date(this.listScheduleFromFloorByWeek[i].time_start),
              end: new Date(this.listScheduleFromFloorByWeek[i].time_finish),
              title: "<b>" + "R." + this.listNameRoom.filter(room => room.id_room == this.listScheduleFromFloorByWeek[i].id_room)[0].name_room,
              color: colors.gray,
              allDay: false,

              resizable: {
                beforeStart: false,
                afterEnd: false
              },
              draggable: false
            }

            this.events.push(this.objScheduleOfFloorByWeek);

          }
          this.refresh.next();
        });

  }
  objScheduleOfFloorByWeek: any;
  getScheduleFloorByWeek(id_Floor: string, dayofweek: string) {
    this.BookingService.getScheduleFromFloorByWeek(id_Floor, dayofweek).subscribe(
      (data: Schedule[]) => {
        //console.log(`All histoty schedule from floor = ${JSON.stringify(data)}`);
        this.listScheduleFromFloorByWeek = data;
        for (var i = 0; i < this.listScheduleFromFloorByWeek.length; i++) {
          this.objScheduleOfFloorByWeek = {
            start: new Date(this.listScheduleFromFloorByWeek[i].time_start),
            end: new Date(this.listScheduleFromFloorByWeek[i].time_finish),
            title: "<b>" + "R." + this.listNameRoom.filter(room => room.id_room == this.listScheduleFromFloorByWeek[i].id_room)[0].name_room,
            color: colors.gray,
            allDay: false,

            resizable: {
              beforeStart: false,
              afterEnd: false
            },
            draggable: false
          }

          this.events.push(this.objScheduleOfFloorByWeek);

        }
        this.refresh.next();
      }
    )
  }

  objScheduleOfRoomByWeek: any;
  getScheduleRoomByWeek(id_Floor: string, id_Room: string, dayofweek: string) {

    this.BookingService.getScheduleFromRoomByWeek(id_Floor, id_Room, dayofweek).subscribe(
      (data: Schedule[]) => {
        // console.log(`All histoty schedule from room = ${JSON.stringify(data)}`);
        this.listScheduleFromRoomByWeek = data;
        for (var i = 0; i < this.listScheduleFromRoomByWeek.length; i++) {
          this.objScheduleOfRoomByWeek = {
            start: new Date(this.listScheduleFromRoomByWeek[i].time_start),
            end: new Date(this.listScheduleFromRoomByWeek[i].time_finish),
            title: "<b>" + "R." + this.listNameRoom.filter(room => room.id_room == this.listScheduleFromRoomByWeek[i].id_room)[0].name_room,
            color: colors.gray,
            allDay: false,

            resizable: {
              beforeStart: false,
              afterEnd: false
            },
            draggable: false
          }

          this.events.push(this.objScheduleOfRoomByWeek);

        }
        this.refresh.next();


      }


    )
  }
  getNameRoom(idBuilding: string, idFloor: string) {
    this.BookingService.getNameRoom(idBuilding, idFloor).subscribe(
      (dataRoom: Room[]) => {

        this.listNameRoom = dataRoom;
        console.log(this.listNameRoom);
      });
    const conbined = forkJoin
  }
  modalAddScheduleOption: NgbModalOptions = {};
  modalAddEvent(): void {
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    this.modalOption.size = "lg";
    this.modalAddReference = this.modal.open(this.modalAddSchedule, this.modalOption);
    this.messagesAddSchedule = null;
  }
  modalOption: NgbModalOptions = {}; // not null!
  handleEvent(action: string, event: CalendarEvent): void {

    this.modalData = { event, action };
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    this.modalOption.size = "lg";
    this.modalReference = this.modal.open(this.modalContent, this.modalOption);
    this.getScheduleById(this.selectedEvent.id.toString());
    this.messagesUpdateSchedule = null;

  }
  setHour() {
    for (let i = 7; i < 24; i++) {
      this.arrayHour.push(i);
    }
  }

  setRentHour() {
    for (let i = 1; i < 6; i++) {
      this.arrayRentHour.push(i);
    }
  }


  getScheduleById(idSchedule: string) {
    let currentSchedule = new Schedule();
    this.BookingService.getScheduleById(idSchedule).subscribe(
      (data: Schedule) => {
        currentSchedule = data;
        this.scheduleUpdate = currentSchedule;
      }
    );
  }
  messagesUpdateSchedule: string;
  updateModal(idBuilding: string, idFloor: string, idRoom: string, dateChoosed: Date, hourChoose: number,
    minuteChoose: string, timeSchedule: number) {
    // if(this.checkTimeLesserCurrent == null)
    console.log(idRoom);

    this.messagesUpdateSchedule = null;
    var minute = Number(minuteChoose);
    var dateStart = new Date(dateChoosed);
    dateStart.setHours(hourChoose);
    dateStart.setMinutes(minute);
    var dateFinish = new Date(dateStart);
    dateFinish.setHours(hourChoose + timeSchedule);
    this.scheduleUpdate.id_building = idBuilding;
    this.scheduleUpdate.id_floor = idFloor;
    this.scheduleUpdate.id_room = idRoom;
    this.scheduleUpdate.time_start = dateStart;
    this.scheduleUpdate.time_finish = dateFinish;
    console.log(this.scheduleUpdate);
    if (idBuilding == null || idFloor == null || idRoom == null) {
      this.messagesUpdateSchedule = "Please enter full information";
    }
    else {
      if (this.role == "Admin") {
        this.BookingService.updateSchedule(this.selectedEvent.id.toString(), this.scheduleUpdate)
          .pipe(
            tap((schedule: Schedule) => console.log(`schedule: ${JSON.stringify(schedule)}`),
              (err: any) => {
                this.messagesUpdateSchedule = err.error.message;
                console.log(err);

              }),
            catchError(error => of(null))
          )
          .subscribe(x => {
            if (this.messagesUpdateSchedule == null) {
              this.modalReference.close();
              this.onSelectRoom(idRoom);
            }
          });
      }
      else if (this.role == "FloorManager") {
        this.BookingFloormanagerService.updateSchedule(this.selectedEvent.id.toString(), this.scheduleUpdate)
          .pipe(
            tap((schedule: Schedule) => console.log(`schedule: ${JSON.stringify(schedule)}`),
              (err: any) => {
                this.messagesUpdateSchedule = err.error.message;
                console.log(err);

              }),
            catchError(error => of(null))
          )
          .subscribe(x => {
            if (this.messagesUpdateSchedule == null) {
              this.modalReference.close();
              this.onSelectRoom(idRoom);
            }
          });
      }
      else if (this.role == "Customer") {
        this.BookingCustomerService.updateSchedule(this.selectedEvent.id.toString(), this.scheduleUpdate)
          .pipe(
            tap((schedule: Schedule) => console.log(`schedule: ${JSON.stringify(schedule)}`),
              (err: any) => {
                this.messagesUpdateSchedule = err.error.message;
                console.log(err);

              }),
            catchError(error => of(null))
          )
          .subscribe(x => {
            if (this.messagesUpdateSchedule == null) {
              this.modalReference.close();
              this.onSelectRoom(idRoom);
            }
          });
      }

    }

  }
  closeModal() {
    if (this.action == "Click event") {
      this.modalReference.close();
    }
    else if (this.action == "Dropped or resized") {
      this.events = this.events.map(iEvent => {
        if (iEvent.id == this.selectedEvent.id) {
          return {
            ...this.selectedEvent,
            start: this.selectedTimeStart,
            end: this.selectedTimeEnd,
          };
        }
        return iEvent;
      });
      this.refresh.next();
      this.modalReference.close();
    }

  }
  closeModalAdd() {
    this.modalAddReference.close();
  }
  CheckScheduleForTimeStart(id: string, start: Date, end: Date): boolean {
    for (let sche of this.events) {
      if (sche.id != id) {
        if (start >= new Date(sche.start) && start < new Date(sche.end)) {
          return true;
        }
      }
    }
    return false;
  }
  CheckScheduleForTimeEnd(id: string, start: Date, end: Date): boolean {
    for (let sche of this.events) {
      if (sche.id != id) {
        if (end > new Date(sche.start) && end <= new Date(sche.end)) {
          return true;
        }
      }
    }
    return false;
  }
  CheckScheduleForTimeFull(id: string, start: Date, end: Date): boolean {
    for (let sche of this.events) {
      if (sche.id != id) {
        if (start < new Date(sche.start) && end >= new Date(sche.end)) {
          return true;
        }
      }
    }
    return false;
  }

  messagesAddSchedule: string;
  addModal(dateChoosed: Date, hourChoose: number,
    minuteChoose: string, timeSchedule: number) {

    this.messagesAddSchedule = null;
    var schedule = new Schedule();

    var minute = Number(minuteChoose);
    var dateStart = new Date(dateChoosed);
    dateStart.setHours(hourChoose);
    dateStart.setMinutes(minute);
    var dateFinish = new Date(dateStart);
    dateFinish.setHours(hourChoose + timeSchedule);
    schedule.id_building = this.idBuilding;
    schedule.id_floor = this.idFloor;
    schedule.id_room = this.idRoom;
    schedule.time_start = dateStart;
    schedule.time_finish = dateFinish;
    schedule.date = dateChoosed;
    schedule.id_personapprove = "";
    schedule.issue = "";
    schedule.person_create = this.user_name;
    schedule.person_update = this.user_name;
    if (this.role == "FloorManager") {
      schedule.status = "waiting approved";
      if (this.managementBuildingFloors.id == this.idBuilding) {
        var filter = this.managementBuildingFloors.floor.filter(x => x.id_floor == this.idFloor)[0];
        if (filter != null)
          schedule.status = "approved";
      }
    }
    console.log(schedule.status);

    var deal = new Deal();
    deal.schedule.push(schedule);
    deal.id_user = this.id_user;
    deal.person_create = this.user_name;
    deal.person_update = this.user_name;
    console.log(deal);

    if (dateChoosed == null || hourChoose == null || minuteChoose == null || timeSchedule == null) {
      this.messagesAddSchedule = "Please enter full information";
    }
    else {
      if (this.role == "FloorManager") {
        this.BookingFloormanagerService.createDealScheduleFloormanager(deal)
          .pipe(
            tap((deal: Deal) => console.log(`deal: ${JSON.stringify(deal)}`),
              (err: any) => {
                this.messagesAddSchedule = err.error.message;
                console.log(err);

              }),
            catchError(error => of(null))
          )
          .subscribe(x => {
            if (this.messagesAddSchedule == null) {
              this.modalAddReference.close();
              this.onSelectRoom(this.idRoom);
            }
          })
      }
      else if (this.role == "Admin") {
        this.BookingService.createDealSchedule(deal)
          .pipe(
            tap((deal: Deal) => console.log(`deal: ${JSON.stringify(deal)}`),
              (err: any) => {
                this.messagesAddSchedule = err.error.message;
                console.log(err);

              }),
            catchError(error => of(null))
          )
          .subscribe(x => {
            if (this.messagesAddSchedule == null) {
              this.modalAddReference.close();   
              this.onSelectRoom(this.idRoom);
            }
          });
      }
      else if (this.role == "Customer") {
        this.BookingCustomerService.createDealScheduleCustomer(deal)
          .pipe(
            tap((deal: Deal) => console.log(`deal: ${JSON.stringify(deal)}`),
              (err: any) => {
                this.messagesAddSchedule = err.error.message;
                console.log(err);

              }),
            catchError(error => of(null))
          )
          .subscribe(x => {
            if (this.messagesAddSchedule == null) {
              this.modalAddReference.close();
              this.listScheduleFromIdUser = [];
              this.getScheduleFromIdUser();
              this.onSelectRoom(this.idRoom);
            }
          })
      }
    }
  }
  deleteModal() {
    this.deleteEvent(this.selectedEvent);
    
  }
  checkManagementBuildingFloors(schedule: Schedule): boolean {
    if (this.role == "Admin") {
      return true;
    }
    else if (this.role == "FloorManager") {
      if (this.managementBuildingFloors.id == schedule.id_building) {
        let filter = this.managementBuildingFloors.floor.filter(x => x.id_floor == schedule.id_floor)[0];
        if (filter != null)
          return true
      }
      else
        return false;
    }
   else if (this.role == "Customer") {
      let filter = this.listScheduleFromIdUser.filter(sche => sche.id_schedule == schedule.id_schedule)[0];
      if( filter != null){
        return true;
      }  
    }
    return false;
  }
  getScheduleFromIdUser() {
    this.BookingCustomerService.getScheduleFromIDUser(this.id_user).subscribe(
      (data: Schedule[]) => {
        this.listScheduleFromIdUser = data;
      }
    )
  }
  setColors(statusSchedule: string, idSchedule: string): any {
    if (this.role == "Customer") {
      if (statusSchedule == "waiting approved" && this.listScheduleFromIdUser.filter(sche => sche.id_schedule == idSchedule)[0] == null)
        return colors.yellow;
      else if (statusSchedule == "waiting approved" && this.listScheduleFromIdUser.filter(sche => sche.id_schedule == idSchedule)[0] != null)
        return colors.yellow;
      else if (statusSchedule == "approved" && this.listScheduleFromIdUser.filter(sche => sche.id_schedule == idSchedule)[0] == null)
        return colors.blue;
      else if (statusSchedule == "approved" && this.listScheduleFromIdUser.filter(sche => sche.id_schedule == idSchedule)[0] != null) {
        console.log(this.role);
        return colors.green;
      }

    }
    else if (this.role == "Admin" || this.role == "FloorManager") {
      if (statusSchedule == "approved")
        return colors.blue
      else if (statusSchedule == "waiting approved")
        return colors.yellow;
    }

  }


}
