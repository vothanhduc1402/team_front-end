import { AuthGuardService } from './../guard/auth-guard.service';
import { Injectable} from '@angular/core';
import { User } from 'app/models/user';
import { Deal } from 'app/models/deal';
import { Schedule } from 'app/models/schedule';
import { Observable,of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Room } from 'app/models/room';

var token = localStorage.getItem("jwt");
var header_object = new HttpHeaders().set("Authorization", "Bearer: " + token)

const httpOptions = { headers: header_object};
@Injectable({
  providedIn: 'root'
})
export class BookingService{
    private apiURL = "http://192.168.1.69:44392/api/roombookedmanagement";
    //private apiURL = "https://localhost:44392/api/roombookedmanagement";

    // private apiURL = "https://localhost:44392/api/accountmanagement"; 

    //listSchedule: Schedule[];

    constructor(private http: HttpClient) { }

    getScheduleFromIdRoom(id_room: string): any{

        const url =  `${this.apiURL}/floorschedule/${id_room}`;    
        return this.http.get(url);
      };
    
    getScheduleFromFloor(id_building:string, id_floor:string): Observable<Schedule[]>{
      const url =  `${this.apiURL}/getschedulefromfloor/${id_building}/${id_floor}`;    
      return this.http.get<Schedule[]>(url);
    }

    getScheduleFromFloorByWeek(id_floor:string, dayofweek: string): Observable<Schedule[]>{
      const url =  `${this.apiURL}/getfloorschedulebyweek/${id_floor}/${dayofweek}`;
      return this.http.get<Schedule[]>(url);
    }

    getScheduleFromRoomByWeek(id_floor:string, id_room: string, dayofweek: string): any{
      const url =  `${this.apiURL}/getroomschedulebyweek/${id_floor}/${id_room}/${dayofweek}`;
      return this.http.get(url);
    }
    
    getNameRoom(id_building:string, id_floor: string):Observable<Room[]>{
      const url =  `${this.apiURL}/getnameroombyfloor/${id_building}/${id_floor}`;
      return this.http.get<Room[]>(url);

    }

    

    createDealSchedule(newDeal: Deal):any{
      
      const url =  `${this.apiURL}/createdeal`;
      console.log(url);
      return this.http.post(url, newDeal);
      
    }
    createSchedule(idDeal: string, schedules: Schedule[]):any{
      const url =  `${this.apiURL}/bookroomfloormanager/${idDeal}`;
      //let bodyschedule = JSON.stringify(schedules);
      return this.http.put(url, schedules);
    }
    deleteScheduleFromFloor(idSchedule: any){
      const url = `${this.apiURL}/deleteschedule/${idSchedule}`;
      console.log(url);
      return this.http.delete(url,httpOptions).pipe(
        tap(_ => console.log(`delete schedule with id: ${idSchedule}`)),
        catchError(err => of(null))
      );
      
    };
    updateStatusDeal(idSchedule: any, status: string){
      const url = `${this.apiURL}/updatestatusdealbyidschedule/${idSchedule}/${status}`;
      console.log(url);
      return this.http.put(url,httpOptions);
      
    }
    getScheduleById(idSchedule: string){
      const url = `${this.apiURL}/getschedulebyid/${idSchedule}`;
      return this.http.get(url,httpOptions);
    }
    updateSchedule(idSchedule: string, schedule: Schedule){
      const url = `${this.apiURL}/updateschedulebyidschedule/${idSchedule}`;
      return this.http.put(url, schedule);
    }

}