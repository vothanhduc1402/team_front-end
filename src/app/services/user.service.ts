import { AuthGuardService } from './../guard/auth-guard.service';
import { Injectable} from '@angular/core';
import { User } from 'app/models/user';
import { Observable,of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders,  } from '@angular/common/http';
import { Building } from 'app/models/building';

var token = localStorage.getItem("jwt");
var header_object = new HttpHeaders().set("Authorization", "Bearer: " + token)

const httpOptions = { headers: header_object};
// console.log(httpOptions);
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiURL = "http://192.168.1.69:44392/api/accountmanagement";

  
  // private apiURL = "https://localhost:44392/api/accountmanagement"; 
  formUser: User;
  list: User[];

  constructor(private http: HttpClient) { }

  // function get all user from api
  getUser(): Observable<User[]>{
    return this.http.get<User[]>(this.apiURL).pipe(
      catchError(error => of([])),
    );
  };

  //function get user by id from api
  getUserfromId(id: string): Observable<User>{

    const url =  `${this.apiURL}/${id}`;

    return this.http.get<User>(url).pipe(
      tap(selectedUser => console.log(`select user = ${JSON.stringify(selectedUser)}`)),
      catchError(err => of(new User()))
    );
  };

  //function search user by name from api
  searchUser(userName?: string): Observable<User>{
    const url = `${this.apiURL}/searchbyname/${userName}`;

    return this.http.get<User>(url);
  };

  //function add user from api
  addUser(newUser: User): Observable<User> {
    return this.http.post<User>(this.apiURL, newUser, httpOptions)
  };

  //function delete user from api
  deleteUser(userId: string): Observable<User>{
    const url = `${this.apiURL}/${userId}`;
    return this.http.delete<User>(url,httpOptions).pipe(
      tap(_ => console.log(`delete user with id: ${userId}`)),
      catchError(error => of(null))
    );
  };

  //function update user from api
  updateUser(user: User): Observable<any>{
    const httpOptions = {headers: new HttpHeaders({'content-type': 'application/json'})};
      return this.http.put(`${this.apiURL}/${user.id}`, user,httpOptions).pipe(
        tap(updateUser => console.log(`update user = ${JSON.stringify(user)}`)),
        catchError(error => of(new User()))
        );  
  }

  ChangePasswordUser(oldPassword: string, newPassWord: string, username: string, updateUser: User){
    const urlWithParamater = `${this.apiURL}/changepassword/${username}/${oldPassword}/${newPassWord}`;
    const httpOptions = {headers: new HttpHeaders({'content-type': 'application/json'})};
      return this.http.put(urlWithParamater, updateUser, httpOptions);
  }

  //function get building from id
  getBuildingfromId(id_user: string): Observable<Building>{
    const url =  `${this.apiURL}/getnamebuildingfloor/${id_user}`;
    return this.http.get<Building>(url).pipe(
      tap(selectedBuilding => console.log(`select building = ${JSON.stringify(selectedBuilding)}`)),
      catchError(() => of(new Building()))
    );
  };

  //function get name floor of building from api
  // getNameAllBuildingFloor(): any{
  //   const url =  this.http.get(`${this.apiURL}/getnameallbuildingfloor`);
  //   return url;
  // };

  getNameAllBuildingFloor(): any{
    const url = `${this.apiURL}/getnameallbuildingfloor`;
    return this.http.get<Building>(url).pipe(
      tap(AllBuildingFloor => console.log(`All Building = ${JSON.stringify(AllBuildingFloor)}`)),
      catchError(() => of(new Building()))
    )
  };
  //function get name all floor from api
  getNameAllFloor(id_building: string): Observable<Building>{
    const url =  `${this.apiURL}/getnameallfloor/${id_building}`;
    return this.http.get<Building>(url).pipe(
      tap(AllNameBuidingFloor => console.log(`allNameFloor = ${JSON.stringify(AllNameBuidingFloor)}`)),
      catchError(() => of(new Building)) 
    );
  };

  getNameBuildingFromIdUser(idUser: string): any{
    const url = `${this.apiURL}/getnamebuildingfloor/${idUser}`;
    return this.http.get(url).pipe(
      tap(BuildingofUser => console.log(`Building of floor manager = ${JSON.stringify(BuildingofUser)}`)),
      catchError(() => of(new User))
    )
  }
}



