import { AuthGuardService } from './../guard/auth-guard.service';
import { Injectable} from '@angular/core';
import { User } from 'app/models/user';
import { Deal } from 'app/models/deal';
import { Schedule } from 'app/models/schedule';
import { Observable,of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';

var token = localStorage.getItem("jwt");
var header_object = new HttpHeaders().set("Authorization", "Bearer: " + token);

const httpOptions = { headers: header_object};
@Injectable({
  providedIn: 'root'
})
export class BookingFloormanagerService {

  private apiURL = "http://192.168.1.69:44392/api/roombookedfloormanager";


    constructor(private http: HttpClient) { }

    createDealScheduleFloormanager(newDeal: Deal):any{
      
      const url =  `${this.apiURL}/createdeal`;
      console.log(url);
      return this.http.post(url, newDeal);
    }
    updateStatusDeal(idSchedule: any, status: string){
      const url = `${this.apiURL}/updatestatusdealbyidschedule/${idSchedule}/${status}`;
      console.log(url);
      return this.http.put(url,httpOptions);
      
    }
    updateSchedule(idSchedule: string, schedule: Schedule){
      const url = `${this.apiURL}/updateschedulebyidschedule/${idSchedule}`;
      return this.http.put(url, schedule);
    }

}
