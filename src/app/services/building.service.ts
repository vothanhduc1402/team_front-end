import { AuthGuardService } from '../guard/auth-guard.service';
import { Injectable} from '@angular/core';
import { Building } from 'app/models/building';
import { Floor } from 'app/models/floor';
import { Room } from 'app/models/room';
import { Observable,of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';

var token = localStorage.getItem("jwt");
var header_object = new HttpHeaders().set("Authorization", "Bearer: " + token)

const httpOptions = { headers: header_object};

@Injectable({
  providedIn: 'root'
})

export class BuildingService{
    //private apiURL = "http://192.168.1.69:44392/api/accountmanagement";
    private apiURL = "http://192.168.1.69:44392/api/buildingmanagement";
    //private apiURL = "https://localhost:44392/api/accountmanagement";

    private apiURLFloor = "http://192.168.1.69:44392/api/floormanagement";
    private apiURLRoom = "http://192.168.1.69:44392/api/roommanagement";

    listBuilding: Building[];

    constructor(private http: HttpClient) { }

    getAllBuilding(): any{

        const url =  `${this.apiURL}`; 
        return this.http.get(url).pipe(
          tap(),
          catchError(err => of(new Building()))
        );
    };

    addBuilding(NewBuilding: Building){
      return this.http.post(this.apiURL, NewBuilding);
    } 
    
    deleteBuilding(buildingId: string):Observable<Building>{
      const UrlWithParameter =  `${this.apiURL}/${buildingId}`;

      return this.http.delete(UrlWithParameter).pipe(
        tap(_ => console.log(`delete building with id: ${buildingId}`)),
        catchError(error => of(null))
      )
    }

    updateBuilding(building: Building){
      return this.http.put(`${this.apiURL}/${building.id}`,building);
    }

    getAllFloorByIDBuilding(id:string): any{
      const url =  `${this.apiURL}/getallfloor/${id}`; 
      return this.http.get(url).pipe(
        tap(allFloor => console.log(`floor= `)),
        catchError(err => of(new Floor()))
      );
    }

    getAllRoomByIDFloor(id_building:string, id_floor: string): any{
      const url =  `${this.apiURLRoom}/${id_building}/${id_floor}`; 
      return this.http.get(url).pipe(
        tap(allFloor => console.log(`floor=`)),
        catchError(err => of(new Room()))
      );
    } 
    
    addFloor(idBuilding: string, newFloor: Floor):Observable<Floor>{
      const url = `${this.apiURLFloor}/addfloor/${idBuilding}`;
      return this.http.post<Floor>(url, newFloor, httpOptions);
    }

    deleteFloor(idBuilding:string, idFloor: string):Observable<Floor>{
      const url = `${this.apiURLFloor}/deletefloor/${idBuilding}/${idFloor}`;
      return this.http.delete<Floor>(url,httpOptions).pipe(
        tap(_ => console.log(`delete floor with id: ${idFloor}`)),
        catchError(error => of(null))
      );
    }

    updateFloor(idBuilding:string, idFloor: string, newfloor: Floor):any{
      const url = `${this.apiURLFloor}/updatefloor/${idBuilding}/${idFloor}`;
      const httpOptions = {headers: new HttpHeaders({'content-type': 'application/json'})};
        return this.http.put(url, newfloor,httpOptions);  
    }

    addRoom(idBuilding: string, idFloor: string, newRoom: Room):Observable<Room>{
      const url = `${this.apiURLRoom}/${idBuilding}/${idFloor}`;
      return this.http.post<Room>(url, newRoom, httpOptions);
    }

    deleteRoom(idBuilding:string,idFloor: string, idRoom: string):Observable<Floor>{
      const url = `${this.apiURLRoom}/${idBuilding}/${idFloor}/${idRoom}`;
      return this.http.delete<Room>(url,httpOptions).pipe(
        tap(_ => console.log(`delete room with id: ${idRoom}`)),
        catchError(error => of(null))
      );
    }

    updateRoom(idBuilding:string, idFloor: string,idRoom: string, newroom: Room):any{
      const url = `${this.apiURLRoom}/${idBuilding}/${idFloor}/${idRoom}`;
      const httpOptions = {headers: new HttpHeaders({'content-type': 'application/json'})};
        return this.http.put(url, newroom,httpOptions);
    }
}