import { AuthGuardService } from './../guard/auth-guard.service';
import { Injectable} from '@angular/core';
import { User } from 'app/models/user';
import { Deal } from 'app/models/deal';
import { Schedule } from 'app/models/schedule';
import { Observable,of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';

var token = localStorage.getItem("jwt");
var header_object = new HttpHeaders().set("Authorization", "Bearer: " + token)

const httpOptions = { headers: header_object};
@Injectable({
  providedIn: 'root'
})
export class BookingCustomerService{

    constructor(private http: HttpClient) { }

    private apiURL = "http://192.168.1.69:44392/api/roombookedcustomer";

    createDealScheduleCustomer(newDeal: Deal):any{
      
        const url =  `${this.apiURL}/createdeal`;
        return this.http.post(url, newDeal);
        
      }

      getScheduleFromIDUser(idUser:string): any{
        const url =  `${this.apiURL}/getschedulebyiduser/${idUser}`;
        return this.http.get(url);
      }

      deleteSchedule(idSchedule: string){
        const url = `${this.apiURL}/deleteschedule/${idSchedule}`;
        return this.http.put(url,httpOptions);
        
      }
      updateSchedule(idSchedule: string, schedule: Schedule){
        const url = `${this.apiURL}/updateschedulecustomer/${idSchedule}`;
        return this.http.put(url, schedule);
      }
}