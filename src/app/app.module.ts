import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { AppComponent, } from './app.component';
import { HomeComponent } from './login/home.component';
import { ErrorComponent } from './error/error.component';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';
import { FloorComponent } from './floor/floor.component';
import { CustomerComponent } from './customer/customer.component';
import { TableListComponent } from './admin/table-list/table-list.component';
import { SidebarAdminComponent } from './admin/sidebar-admin/sidebar-admin.component';
import { NavbarAdminComponent } from './admin/navbar-admin/navbar-admin.component';
import { AddUserComponent, TrackCapsDirective } from './admin/add-user/add-user.component';
import { UserDetailComponent } from './admin/user-detail/user-detail.component';
import { UserUpdateComponent } from './admin/user-update/user-update.component';

import { ScheduleCalendarComponent } from './admin/schedule-calendar/schedule-calendar.component';
import { BuildingsComponent } from './booked_Room/buildings/buildings.component';
import { FloorsComponent } from './booked_Room/floors/floors.component';
import { RoomsComponent } from './booked_Room/rooms/rooms.component';
import { MngBuildingsComponent } from './admin/manage-buildings/mng-buildings/mng-buildings.component';
import { MngFloorsComponent } from './admin/manage-buildings/mng-floors/mng-floors.component';
import { MngRoomsComponent } from './admin/manage-buildings/mng-rooms/mng-rooms.component';
import { MngFloorsAddComponent } from './admin/manage-buildings/mng-floors/mng-floors-add/mng-floors-add.component';
import { MngFloorsUpdateComponent } from './admin/manage-buildings/mng-floors/mng-floors-update/mng-floors-update.component';
import { SidebarFloorComponent } from './floor/sidebar-floor/sidebar-floor.component';
import { NavbarFloorComponent } from './floor/navbar-floor/navbar-floor.component';
import { FloorMngFloorsComponent } from './floor/floor-manager-buildings/floor-mng-floors/floor-mng-floors.component';
import { FloorMngRoomsComponent } from './floor/floor-manager-buildings/floor-mng-rooms/floor-mng-rooms.component';
import { MngRoomsAddComponent } from './admin/manage-buildings/mng-rooms/mng-rooms-add/mng-rooms-add.component';
import { MngRoomsUpdateComponent } from './admin/manage-buildings/mng-rooms/mng-rooms-update/mng-rooms-update.component';
import { MngBuildingAddComponent } from './admin/manage-buildings/mng-buildings/mng-building-add/mng-building-add.component';
import { AddScheduleCalendarComponent } from './admin/add-schedule-calendar/add-schedule-calendar.component';
import { MngBuildingUpdateComponent } from './admin/manage-buildings/mng-buildings/mng-building-update/mng-building-update.component';
import { NavbarCustomerComponent } from './customer/navbar-customer/navbar-customer.component';
import { SidebarCustomerComponent } from './customer/sidebar-customer/sidebar-customer.component';
import { FloorMngRoomsAddComponent } from './floor/floor-manager-buildings/floor-mng-rooms/floor-mng-rooms-add/floor-mng-rooms-add.component';
import { FloorMngRoomsUpdateComponent } from './floor/floor-manager-buildings/floor-mng-rooms/floor-mng-rooms-update/floor-mng-rooms-update.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { DashboardAdminComponent } from './admin/dashboard-admin/dashboard-admin.component';
import { DetailCustomerComponent } from './customer/detail-customer/detail-customer.component';
import { ScheduleCalendarCustomerComponent } from './customer/schedule-calendar-customer/schedule-calendar-customer.component';
import { BookingScheduleComponent } from './booking-schedule/booking-schedule.component';
import { FloormanagerScheduleCalendarComponent } from './floor/floormanager-schedule-calendar/floormanager-schedule-calendar.component';
import { ScheduleApproveComponent } from './admin/schedule-approve-admin/schedule-approve.component';
import { FloormanagerScheduleApproveComponent } from './floor/floormanager-schedule-approve/floormanager-schedule-approve.component';





@NgModule({
  declarations: [
    AppComponent,
    TrackCapsDirective,
    HomeComponent,
    ErrorComponent,
    UserComponent,
    AdminComponent,
    FloorComponent,
    CustomerComponent,
    TableListComponent,
    SidebarAdminComponent,
    NavbarAdminComponent,
    AddUserComponent,
    UserDetailComponent,
    UserUpdateComponent,
    ScheduleCalendarComponent,
    ScheduleCalendarCustomerComponent,
    BuildingsComponent,
    FloorsComponent,
    RoomsComponent,
    MngBuildingsComponent,
    MngFloorsComponent,
    MngRoomsComponent,
    MngFloorsAddComponent,
    MngFloorsUpdateComponent,
    SidebarFloorComponent,
    NavbarFloorComponent,
    FloorMngFloorsComponent,
    FloorMngRoomsComponent,
    MngRoomsAddComponent,
    MngRoomsUpdateComponent,
    MngBuildingAddComponent,
    AddScheduleCalendarComponent,
    MngBuildingUpdateComponent,
    NavbarCustomerComponent,
    SidebarCustomerComponent,
    FloorMngRoomsAddComponent,
    FloorMngRoomsUpdateComponent,
    DashboardAdminComponent,
    DetailCustomerComponent,
    ScheduleCalendarCustomerComponent,
    BookingScheduleComponent,
    FloormanagerScheduleCalendarComponent,
    ScheduleApproveComponent,
    FloormanagerScheduleApproveComponent,
  ],

  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AngularFontAwesomeModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModalModule,
    ReactiveFormsModule,   
    FlatpickrModule.forRoot(),
    NgbModalModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    })
  ],
  providers: [FormsModule],
              // SweetAlertService,
  bootstrap: [AppComponent],
  entryComponents: [AddUserComponent, 
                    UserDetailComponent, 
                    UserUpdateComponent, 
                    MngFloorsAddComponent, 
                    MngFloorsUpdateComponent, 
                    MngRoomsAddComponent, 
                    MngRoomsUpdateComponent,
                    AddScheduleCalendarComponent,
                    MngBuildingAddComponent,
                    MngBuildingUpdateComponent,
                    FloorMngRoomsAddComponent,
                    FloorMngRoomsUpdateComponent,
                    ScheduleCalendarCustomerComponent
  ],
})
export class AppModule { }
