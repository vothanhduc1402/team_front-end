import { Component, OnInit, Inject } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { Room } from 'app/models/room';
import { GetUserNameService } from 'app/shared/get-user-name.service';
import { BuildingService } from 'app/services/building.service';
import  Swal  from 'sweetalert2';
import { catchError, tap, } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-floor-mng-rooms-add',
  templateUrl: './floor-mng-rooms-add.component.html',
  styleUrls: ['./floor-mng-rooms-add.component.css']
})
export class FloorMngRoomsAddComponent implements OnInit {

  nameroom = new FormControl('',[Validators.required]);
  sizeroom = new FormControl('',[Validators.required]);
  roomformGroup: FormGroup;
  arrayId : string[] = [];
  idbuilding : string;
  idfloor : string;
  listRoom: Room[] = [];
  messageFromService: string = null;
  
  constructor(private dialogRef: MatDialogRef<FloorMngRoomsAddComponent>,
              private formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) data, 
              private getUserNameService:GetUserNameService,
              private buildingService : BuildingService,
              private router: Router ) {
      this.arrayId = data;
      this.idbuilding = this.arrayId[0];
      this.idfloor = this.arrayId[1];
      }

  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.roomformGroup = this.formBuilder.group({
      nameroom : ['',[Validators.required]],
      sizeroom : ['',[Validators.required]]
    });
  }

  onSave(name_room: string, size: string){
    this.messageFromService = null;
    var newRoom : Room = new Room();
    newRoom.name_room = name_room.trim();
    newRoom.size = size.trim();
    newRoom.person_create = this.getUserNameService.GetUserNameFromLogin();
    newRoom.time_create = new Date();
    this.buildingService.addRoom(this.idbuilding,this.idfloor, newRoom).pipe(
      tap((room: Room) => console.log(`inserted room = ${JSON.stringify(room)}`),
      (err) => this.messageFromService = err.error.message),
      catchError(error => of(new Room()))
    ).subscribe( insertedRoom =>  {
      this.listRoom.push(insertedRoom);
      if(this.messageFromService === null){
        this.onClose(); 
        Swal.fire({
          type: 'success',
          title: 'Your work has been saved',
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigate([`/floormanager/floor-manage-floors/${this.idbuilding}/${this.idfloor}`]);
      }      
    });    
  }

  onClose(){
    this.dialogRef.close();
  }
}
