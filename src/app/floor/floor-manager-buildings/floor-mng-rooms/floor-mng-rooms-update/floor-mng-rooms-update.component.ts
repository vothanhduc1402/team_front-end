import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { BuildingService } from 'app/services/building.service';
import { GetUserNameService } from 'app/shared/get-user-name.service';
import { Room } from 'app/models/room';
import {FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { catchError, tap, } from 'rxjs/operators';
import { of } from 'rxjs';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-floor-mng-rooms-update',
  templateUrl: './floor-mng-rooms-update.component.html',
  styleUrls: ['./floor-mng-rooms-update.component.css']
})
export class FloorMngRoomsUpdateComponent implements OnInit {

  arrayData : any[]=[];
  idBuilding : string;
  idFloor : string;
  room = new Room();
  nameroom = new FormControl('',[Validators.required]);
  size = new FormControl('',[Validators.required]);
  roomformGroup: FormGroup;
  messageFromService: string = null;

  constructor(private dialog: MatDialog, 
              private buildingService : BuildingService, 
              private dialogRef: MatDialogRef<FloorMngRoomsUpdateComponent>,
              @Inject(MAT_DIALOG_DATA) data, 
              private getUserNameService: GetUserNameService,
              private formBuilder: FormBuilder) {
      this.arrayData = data; 
      this.idBuilding = this.arrayData[0];
      this.idFloor = this.arrayData[1];
      this.room = this.arrayData[2];
    }

  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.roomformGroup = this.formBuilder.group({
      nameroom : ['',[Validators.required]],
      size : ['',[Validators.required]]
    });
  }

  onUpdate(name_room : string, size: string){ 
    this.messageFromService = null;
    console.log(this.arrayData);  
    this.room.name_room = name_room;
    this.room.size = size;
    this.room.person_update = this.getUserNameService.GetUserNameFromLogin();
    this.room.time_update = new Date();
    this.buildingService.updateRoom(this.idBuilding, this.idFloor, this.room.id_room, this.room).pipe(
      tap(updateRoom => console.log(`update room = ${JSON.stringify(updateRoom)}`),
      (err) => this.messageFromService = err.error.message),
      catchError(error => of(new Room()))
      ).subscribe(() => {
        if(this.messageFromService === null){
          this.onClose();
          Swal.fire({
            type: 'success',
            title: 'Your work has been saved',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
  }

  onClose(){
    this.dialogRef.close();
  }

}
