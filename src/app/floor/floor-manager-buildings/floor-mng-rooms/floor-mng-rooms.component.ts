import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FloorMngRoomsAddComponent } from './floor-mng-rooms-add/floor-mng-rooms-add.component';
import { Room } from 'app/models/room';
import { FloorMngRoomsUpdateComponent } from './floor-mng-rooms-update/floor-mng-rooms-update.component';
import { BuildingService } from 'app/services/building.service';
import { GetUserIdService } from 'app/shared/get-user-id.service';
import { Building } from 'app/models/building';
import { UserService } from 'app/services/user.service';
import { Floor } from 'app/models/floor';
import { Router, ActivatedRoute } from '@angular/router';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-floor-mng-rooms',
  templateUrl: './floor-mng-rooms.component.html',
  styleUrls: ['./floor-mng-rooms.component.css']
}) 
export class FloorMngRoomsComponent implements OnInit {

  userId = this.getUserIdService.GetUserIdFromLogin();
  idbuilding : string;
  listRoom : Room[];
  idfloor :string;
  arrayId :string[]=[];
  arrayData :any[]=[];

  constructor(private dialog: MatDialog,
              private getUserIdService: GetUserIdService,
              private userService: UserService,
              private buildingService: BuildingService,
              private ActivatedRoute: ActivatedRoute, 
              private router: Router) { }

  ngOnInit() {
    this.idbuilding = this.ActivatedRoute.snapshot.params['id'];
    this.idfloor = this.ActivatedRoute.snapshot.params['idfloor'];
    this.getAllRoomFromFloor(this.idbuilding, this.idfloor);
  }

  getAllRoomFromFloor(idbuilding:string, idfloor:string){
    this.buildingService.getAllRoomByIDFloor(idbuilding,idfloor).subscribe(
      (data: Room[]) => {
        this.listRoom = data;
        console.log(`All Room = ${JSON.stringify(data)}`);    
      }
    )    
  }

  AddRoom(){
    this.arrayId = [];
    this.arrayId.push(this.idbuilding);
    this.arrayId.push(this.idfloor);
    console.log(this.arrayId);
    const dialogconfig = new MatDialogConfig();
    dialogconfig.disableClose = true;
    dialogconfig.autoFocus = true;
    dialogconfig.data = this.arrayId;
    this.dialog.open(FloorMngRoomsAddComponent, dialogconfig);
  }

  UpdateRoom(room: Room){
    this.arrayData = [];
    this.arrayData.push(this.idbuilding);
    this.arrayData.push(this.idfloor);
    this.arrayData.push(room);
    console.log(this.arrayData);
    const dialogconfig = new MatDialogConfig();
    dialogconfig.disableClose = true;
    dialogconfig.autoFocus = false;
    dialogconfig.data = this.arrayData;
    this.dialog.open(FloorMngRoomsUpdateComponent, dialogconfig);
  }

  DeleteRoom(idroom: string){
    console.log(idroom);
    this.buildingService.deleteRoom(this.idbuilding,this.idfloor, idroom)
    .subscribe(_ => {
      this.listRoom = this.listRoom.filter(eachRoom => eachRoom.id_room !== idroom);
    });   
  }

  alertFunction(idRoom: string){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

      if (result.value) {
        this.DeleteRoom(idRoom);
        Swal.fire(
          'Deleted!',
          'Your building has been deleted.',
          'success'
        )
      }
    })
  }
}
