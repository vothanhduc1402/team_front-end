import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { Building } from 'app/models/building';
import { GetUserIdService } from 'app/shared/get-user-id.service';
import { Floor } from 'app/models/floor';

@Component({
  selector: 'app-floor-mng-floors',
  templateUrl: './floor-mng-floors.component.html',
  styleUrls: ['./floor-mng-floors.component.css']
})
export class FloorMngFloorsComponent implements OnInit {

  userId = this.GetUserIdService.GetUserIdFromLogin();
  building : Building;
  idbuilding : string;
  floor : Floor[];
  arrFloor : Floor[];
  constructor(private userService: UserService, private GetUserIdService: GetUserIdService) { }

  ngOnInit() {
    this.getNameBuildingFromIdUser(this.userId);
  }

  getNameBuildingFromIdUser(idUser: string): any{
    this.userService.getNameBuildingFromIdUser(idUser).subscribe(
      (data: Building)=> {
        this.building = data;
        console.log(`Building of user = ${JSON.stringify(data)}`);
        this.floor = this.building.floor;
        console.log(`All floor = ${JSON.stringify(this.floor)}`);
        this.arrFloor = this.floor;
      }
    );
  }
}
