import { Injectable } from '@angular/core';
import { JwtHelper } from "angular2-jwt";

var userId: string;
@Injectable({
  providedIn: 'root'
})
export class GetUserIdService {

  constructor(private jwtHelper: JwtHelper) { }

  GetUserIdFromLogin(){
    let token = localStorage.getItem("jwt");
    let tokendecode = this.jwtHelper.decodeToken(token);    
    userId = tokendecode.unique_name; 
    return userId;
  }
}
