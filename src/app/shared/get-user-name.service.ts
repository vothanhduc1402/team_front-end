import { Injectable } from '@angular/core';
import { JwtHelper } from "angular2-jwt";

var UserName :string;

@Injectable({
  providedIn: 'root'
})


export class GetUserNameService {
  
   
  constructor(private jwtHelper: JwtHelper) { }

  GetUserNameFromLogin(){
    let token = localStorage.getItem("jwt");
    let tokendecode = this.jwtHelper.decodeToken(token);    
    UserName = tokendecode.actort; 
    return UserName;
  }
}
 