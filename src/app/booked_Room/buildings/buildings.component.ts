import { Component, OnInit } from '@angular/core';
import  {Building} from '../../models/building';
import  {BuildingService} from '../../services/building.service'; 

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.css']
})
export class BuildingsComponent implements OnInit {

  listBuilding: Building[];

  constructor(private BuildingService: BuildingService) { }

  ngOnInit() {
    this.getAllBuildingFromServices();
  }
  
  getAllBuildingFromServices(){
    this.BuildingService.getAllBuilding().subscribe(
      (data:Building[]) => {
        this.listBuilding = data;
        console.log(`List building = ${JSON.stringify(this.listBuilding)}`);
      }

    )
  }

}
