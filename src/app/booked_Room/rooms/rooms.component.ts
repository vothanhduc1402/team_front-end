import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';

import  {BuildingService} from '../../services/building.service'; 
import { Room } from 'app/models/room';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  id_building : string;
  id_floor: string;
  listRoom: Room[];

  constructor(private _ActivatedRoute: ActivatedRoute, private _router: Router, private _BuildingService: BuildingService ) { }

  ngOnInit() {
      this.id_building = this._ActivatedRoute.snapshot.params['id_building'];
      this.id_floor = this._ActivatedRoute.snapshot.params['id_floor'];
      this.getAllRoomByIDFloor(this.id_building, this.id_floor);
  }

  getAllRoomByIDFloor(id_building : string, id_floor: string){
    this._BuildingService.getAllRoomByIDFloor(id_building, id_floor).subscribe(
      (data:Room[]) => {
        this.listRoom = data;
        console.log(`List floor = ${JSON.stringify(this.listRoom)}`);
      }

    )
  }

}
