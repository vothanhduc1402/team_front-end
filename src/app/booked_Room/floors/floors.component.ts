import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';

import  {BuildingService} from '../../services/building.service'; 
import { Floor } from 'app/models/floor';
 

@Component({
  selector: 'app-floors',
  templateUrl: './floors.component.html',
  styleUrls: ['./floors.component.css']
})
export class FloorsComponent implements OnInit {

  id:string;
  listFloor:Floor[];

  constructor(private _ActivatedRoute: ActivatedRoute, private _router: Router, private _BuildingService: BuildingService ) { }

  ngOnInit() {
    this.id = this._ActivatedRoute.snapshot.params['id'];
    this.getAllFloorByIDBuilding(this.id);
  }

  getAllFloorByIDBuilding(id_building: string){
    console.log(id_building);
    
    this._BuildingService.getAllFloorByIDBuilding(id_building).subscribe(
      (data:Floor[]) => {
        this.listFloor = data;
        console.log(`List floor = ${JSON.stringify(this.listFloor)}`);
      }

    )
  }
  

}
