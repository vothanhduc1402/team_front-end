
import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';


import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { AppComponent } from './app.component';
import { HomeComponent } from './login/home.component';
import { ErrorComponent } from './error/error.component';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';
import { FloorComponent } from './floor/floor.component';
import { CustomerComponent } from './customer/customer.component';
import { TableListComponent } from './admin/table-list/table-list.component';
import { SidebarAdminComponent } from './admin/sidebar-admin/sidebar-admin.component';
import { NavbarAdminComponent } from './admin/navbar-admin/navbar-admin.component';
import { RoomManagementComponent } from './admin/room-management/room-management.component';
import { AddUserComponent } from './admin/add-user/add-user.component';
import { UserDetailComponent } from './admin/user-detail/user-detail.component';
import { UserUpdateComponent } from './admin/user-update/user-update.component';
import { ScheduleCalendarComponent } from './admin/schedule-calendar/schedule-calendar.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    UserComponent,
    AdminComponent,
    FloorComponent,
    CustomerComponent,
    TableListComponent,
    SidebarAdminComponent,
    NavbarAdminComponent,
    RoomManagementComponent,
    AddUserComponent,
    UserDetailComponent,
    UserUpdateComponent,
    ScheduleCalendarComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AddUserComponent, UserDetailComponent, UserUpdateComponent],
})
export class AppModule { }
