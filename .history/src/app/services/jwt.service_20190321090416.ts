import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  private apiURL = "http://192.168.1.69:44392/api/accountmanagement";

  
  //private apiURL = "https://localhost:44392/api/accountmanagement"; 
  constructor(private httpClient: HttpClient) { }

  login(username:string, password:string) {
    return this.httpClient.post<{access_token:  string}>('apiURL', {username, password}).pipe(tap(res => {
    localStorage.setItem('access_token', res.access_token);
    }))
  };

  public getloggedIn(): boolean{
    return localStorage.getItem('access_token') !== null;
  }
}
