import { Injectable } from '@angular/core';
import { HttpInterceptor,HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const Token = localStorage.getItem("jwt");
        if(Token){
            const cloned = req.clone({
                headers: req.headers.set("Authentication","Bearer " + Token)
            });

            return next.handle(cloned);
            
        }
        else{
            return next.handle(req);
        }
        // throw new Error("Method not implemented.");
    }
}