import { Injectable } from '@angular/core';
import { User } from 'app/models/user';
import { Observable,of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders,  } from '@angular/common/http';

const httpOptions = { headers: new HttpHeaders({'Content-type': 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiURL = "http://192.168.1.69:44392/api/accountmanagement";

  
  //private apiURL = "https://localhost:44392/api/accountmanagement"; 
  formUser: User;
  list: User[];

  constructor(private http: HttpClient) { }

  // function get all user from api
  getUser(): Observable<User[]>{
    return this.http.get<User[]>(this.apiURL).pipe(
      tap(recivedUser => console.log(`receivedUser = ${JSON.stringify(recivedUser)}`)),
      catchError(error => of([])),
    );
  };

  //function get user by id from api
  getUserfromId(id: string): Observable<User>{

    const url =  `${this.apiURL}/${id}`;

    return this.http.get<User>(url).pipe(
      tap(selectedUser => console.log(`select user = ${JSON.stringify(selectedUser)}`)),
      catchError(err => of(new User()))
    );
  };

  //function search user by name from api
  searchUser(userName?: string): Observable<User>{
    const url = `${this.apiURL}/searchbyname/${userName}`;

    return this.http.get<User>(url).pipe(
      tap(searchUser => console.log(`search user = ${JSON.stringify(searchUser)}`))
    );
  };

  //function add user from api
  addUser(newUser: User): Observable<User> {
    return this.http.post<User>(this.apiURL, newUser, httpOptions).pipe(
      tap((user: User) => console.log(`inserted user = ${JSON.stringify(user)}`)),
      catchError(error => of(new User()))
    );
  };

  //function delete user from api
  deleteUser(userId: string): Observable<User>{
    const url = `${this.apiURL}/${userId}`;
    return this.http.delete<User>(url,httpOptions).pipe(
      tap(_ => console.log(`delete user with id: ${userId}`)),
      catchError(error => of(null))
    );
  }
}
