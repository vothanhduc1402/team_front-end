import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/auth.interceptor';
import { JwtHelper } from 'angular2-jwt'
import { AuthGuardService } from './guard/auth-guard.service';


import { HomeComponent } from './login/home.component';
import { ErrorComponent } from './error/error.component';
import { AdminComponent } from './admin/admin.component';
import { FloorComponent } from './floor/floor.component';
import { CustomerComponent } from './customer/customer.component';
import { TableListComponent } from './admin/table-list/table-list.component';
import { RoomManagementComponent } from './admin/room-management/room-management.component';
import { ScheduleCalendarComponent } from './admin/schedule-calendar/schedule-calendar.component';




const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },

  {
    path: 'admin', canActivate: [AuthGuardService],
    //  canActivate: [RoleGuard],
    // data: {
    //   expectedRole: 'admin'
    // },
    children: [
    { path: 'user-management', component: TableListComponent,  },
    { path: 'room-management', component: RoomManagementComponent,  },
    {path: 'Schedule-Calendar', component: ScheduleCalendarComponent},
  ],
    component: AdminComponent
  },
  {
    path: 'floormanager',  canActivate: [AuthGuardService],
    component: FloorComponent
  },
  {
    path: 'customer',  canActivate: [AuthGuardService],
    component: CustomerComponent
  },
  {
    path: '**',
    component: ErrorComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    JwtHelper, 
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi : true,
    }
  
  ],
})
export class AppRoutingModule { }
