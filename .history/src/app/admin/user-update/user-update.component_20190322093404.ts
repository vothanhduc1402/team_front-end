import { Component, OnInit,Inject } from '@angular/core';
import { User } from 'app/models/user';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from 'app/services/user.service';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  floor = new FormControl();
  floors: string[] = ['1','2','3','4','5','6','7','8','9','10'];
  levels: string[] = ['Admin', 'FloorManager', 'Customer'];
  buildings: string[] = ['toanha4tang', 'toanha8tang', 'toanha10tang','toanha11tang'];
  selectedValue = null;

  // disableSelect = new FormControl(false);

  // public selectedValue: string ="";
  // setDis:boolean = true;

  

  user: User;
  constructor(private dialogRef:  MatDialogRef<UserUpdateComponent>,
             @Inject(MAT_DIALOG_DATA) data:any,
             private userService: UserService) { this.user=data}

  ngOnInit() {
  }

  onUpdate(user_new: User,password:string, name:string,
    email:string, phone:string, birthday:Date,level: string, id_building: string, id_floors : string[]=[]){
    user_new.password = password;
    user_new.name = name;
    user_new.email = email;
    user_new.phone = phone;
    user_new.birthday = birthday;
    user_new.level = level;
    user_new.id_building = id_building;
    user_new.id_floors = id_floors;
    this.userService.updateUser(this.user).subscribe(() => this.onClose());
  }

  onClose(){
    this.dialogRef.close();
  }
}
