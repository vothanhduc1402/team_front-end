import { UserService } from './../../services/user.service';
import { User } from './../../models/user';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {EMPTY} from 'rxjs';
import { filter,switchMap, debounceTime, catchError } from 'rxjs/operators';
@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

  findControl = new FormControl();
  user : User = null;
  error: boolean = false;
  lists: User[] = [];
  listsSearch: User[] = [];

  constructor(private userService: UserService) { }

  
  getUserFromService(): void{
    this.userService.getUser().subscribe(updatedUser => this.lists = updatedUser);
  }
  ngOnInit() {
    this.getUserFromService();
    this.findControl.valueChanges
      .pipe(
        // Filter if less than two characters are entered
        filter (value => value.length>= 0),
        
        // Set the delay to one second
        debounceTime (100),

        // Requesting user data
        
        switchMap (value =>
          this.userService.searchUser (value) .pipe (
            // Error processing
            catchError (err => {
              this.user = null;
              this.error = true;
              return EMPTY;
            })
          )
        )
      )
      // Get the data
      .subscribe (user => {
       this.lists = null;
       this.user = user;        
        this.error = false;
      });
  }

  delete(userId: string): void {
    this.userService.deleteUser(userId).subscribe(_ => {
      this.lists = this.lists.filter(eachUser => eachUser.id !== userId);
    });
  };

  alertFunction(userId: string){
    // let userId : string;
    if(confirm('Are you sure to delete this user?')){
      return this.delete(userId);
    }
    else{
      return false;
    }
  }
}
