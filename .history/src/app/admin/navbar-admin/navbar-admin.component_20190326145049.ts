import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.css']
})
export class NavbarAdminComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  Logout(){
    let token = localStorage.getItem('jwt');

    console.log(token);

    let removeToken = localStorage.removeItem('jwt');

    console.log(removeToken);
    this.router.navigate[' '];
  }
}
