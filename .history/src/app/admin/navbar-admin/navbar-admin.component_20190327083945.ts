import { AuthGuardService } from './../../guard/auth-guard.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.css']
})
export class NavbarAdminComponent implements OnInit {

  constructor(private router: Router, private authGuard: AuthGuardService) { }

  ngOnInit() {
  
  }

  Logout(){
    let token = localStorage.getItem('jwt');
    localStorage.removeItem('jwt');
    this.authGuard.canActivate();

  }
}
