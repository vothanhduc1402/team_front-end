import { Component, OnInit, Input } from '@angular/core';
import { User } from 'app/models/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  @Input() childData = User;

  constructor() { }

  ngOnInit() {
  }

}
