import { catchError } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Component, OnInit} from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material";
import { UserService } from 'app/services/user.service';
import { User } from 'app/models/user';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  
  // floors = new FormControl();
  // floorList : string[] = ['Tầng 1','Tầng 2','Tầng 3','Tầng 4','Tầng 5','Tầng 6','Tầng 7','Tầng 8','Tầng 9','Tầng 10'];
  floors = new FormControl();
  floorsList: string[] = ['Tầng 1','Tầng 2','Tầng 3','Tầng 4','Tầng 5','Tầng 6','Tầng 7','Tầng 8','Tầng 9','Tầng 10'];
  lists: User[] = [];
  
  constructor(private dialog: MatDialog, 
              private userService: UserService,
              private toastService: ToastrService ) { }

 
  ngOnInit() { }

  onClose(){
    this.dialog.closeAll();
  }

  onSave(username: string, 
    password: string, 
    level: string, 
    id_building: string,
    id_floors : string[]=[], 
    name: string, 
    email: string, 
    phone: string, 
    birthday: Date){

      var newUser : User = new User();

      newUser.username = username;
      newUser.password = password;
      newUser.level = level;
      newUser.id_building = id_building;
  
      //newUser.id_buiding.push(id_building);
      newUser.id_floors = id_floors;
      newUser.id = "";
      newUser.name = name;
      newUser.email = email;
      newUser.phone = phone;
      newUser.birthday = birthday;
  
      this.userService.addUser(newUser)
      .subscribe(insertedUser => {
        this.lists.push(insertedUser);
      }),catchError(err => []);
  }

  AlertSucess(from, align){
    if(this.onSave)
    this.toastService.info('<span class="now-ui-icons ui-1_bell-53"></span> Welcome to <b>Now Ui Dashboard</b> - a beautiful freebie for every web developer.', '', {
      timeOut: 8000,
      closeButton: true,
      enableHtml: true,
      toastClass: "alert alert-info alert-with-icon",
      positionClass: 'toast-' + from + '-' +  align
    });
    else {
      return false;
    }
  }

}
