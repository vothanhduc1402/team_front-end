import { FormControl } from '@angular/forms';
import { Component, OnInit} from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  
  // floors = new FormControl();
  // floorList : string[] = ['Tầng 1','Tầng 2','Tầng 3','Tầng 4','Tầng 5','Tầng 6','Tầng 7','Tầng 8','Tầng 9','Tầng 10'];
  floors = new FormControl();
  floorsList: string[] = ['Tầng 1','Tầng 2','Tầng 3','Tầng 4','Tầng 5','Tầng 6','Tầng 7','Tầng 8','Tầng 9','Tầng 10'];
  constructor(private dialog: MatDialog) { }

  ngOnInit() { }

  onClose(){
    this.dialog.closeAll();
  }

}
