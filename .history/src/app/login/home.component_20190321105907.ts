import { Router } from "@angular/router";
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {HttpClient, HttpHeaders,  } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private apiURL = "http://192.168.1.69:44392/api/accountmanagement";
  invalidLogin: boolean;


  constructor(private http: HttpClient, private router: Router, private headers: Headers) { }

  ngOnInit() {
  }

  login(form: NgForm){
    let credentials = JSON.stringify(form.value);
    this.http.post(this.apiURL, credentials, {
      headers: new Headers({
        "Content-Type": "application/json"
      }).subscribe(response => {
        let token = (<any>response).token;
        localStorage.setItem("jwt", token);
        this.invalidLogin = false;
        this.router.navigate(["/"]);
      })
    })
  }
}
