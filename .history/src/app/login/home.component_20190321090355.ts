import { Component, OnInit } from '@angular/core';
import { JwtService } from 'app/services/jwt.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private jwtService: JwtService) { }

  ngOnInit() {
  }

  onSummit(userName: string, passWord: string){
      this.jwtService.login(userName,passWord);
  }
}
