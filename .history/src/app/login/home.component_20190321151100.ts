import { Router } from "@angular/router";
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {HttpClient, HttpHeaders,  } from '@angular/common/http';
import { JwtHelper } from 'angular2-jwt';

const httpOptions = { headers: new HttpHeaders({'Content-type': 'application/json'})};
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private apiURL = "http://192.168.1.69:44392/api/accountmanagement/authenticate";
  invalidLogin: boolean;


  constructor(private http: HttpClient, private router: Router, private jwtHelper: JwtHelper) { }

  ngOnInit() {
  }

  login(username: string, password: string) {
    this.http.post(this.apiURL,{username,password}, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      let token = (<any>response).token;
      localStorage.setItem("jwt", token);
      this.invalidLogin = false;
      var b = this.jwtHelper.decodeToken(token);
      console.log(b.role);
        if(token.role === 'Admin'){
          
          this.router.navigate(["/admin"]);
        }
         
        else
          this.router.navigate(["/customer"]);
      
    }, err => {
      this.invalidLogin = true;
    });
  }
}
