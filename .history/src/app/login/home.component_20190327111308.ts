import { Router } from "@angular/router";
import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse  } from '@angular/common/http';
import { JwtHelper } from 'angular2-jwt';

const httpOptions = { headers: new HttpHeaders({'Content-type': 'application/json'})};
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private apiURL = "http://192.168.1.69:44392/api/accountmanagement/authenticate";

  invalidLogin: boolean;
  ErrorFromServer: string;


  constructor(private http: HttpClient, private router: Router, private jwtHelper: JwtHelper) { }

  ngOnInit() {
  }

  login(username?: string, password?: string) {
    this.http.post(this.apiURL,{username,password}, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
  
      })
    }).subscribe(response => {

      let token = (<any>response).token;
      localStorage.setItem("jwt", token);
      this.invalidLogin = false;
      var tokendecode = this.jwtHelper.decodeToken(token);
      // console.log(tokendecode.role);
      
      switch(tokendecode.role){
        case "Admin": {
          this.router.navigate(["/admin"]);
          break;
        }
        case "Customer": {
          this.router.navigate(["/customer"]);
          break;
        }
        case "FloorManager":{
          this.router.navigate(["/floormanager"]);
          break;
        }
      }
    }, err => {
       this.ErrorFromServer = err.error;
    
      this.invalidLogin = true;
       
    });
  }
}
