import { Router } from "@angular/router";
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {HttpClient, HttpHeaders,  } from '@angular/common/http';

const httpOptions = { headers: new HttpHeaders({'Content-type': 'application/json'})};
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private apiURL = "http://192.168.1.69:44392/api/accountmanagement/authenticate";
  invalidLogin: boolean;


  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  login(username: string, password: string) {
    this.http.post(this.apiURL,{username,password}, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      let token = (<any>response).token;
      localStorage.setItem("jwt", token);
      this.invalidLogin = false;
      if(token.role === "admin"){
        this.router.navigate(["/admin"]);
      }
      else{
        
      }
       
     
    }, err => {
      this.invalidLogin = true;
    });
  }
}
